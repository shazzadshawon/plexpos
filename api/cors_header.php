<?php
	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Credentials: true");
	header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS'); 
	header('Access-Control-Allow-Headers: X-Requested-With, content-type, X-Token, x-token');
	header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
?>