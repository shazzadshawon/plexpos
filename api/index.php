<?php
// set up autoloader
require __DIR__.'/vendor/autoload.php';

// configure database
$dsn = 'mysql:dbname=kaporchu_kc;host=localhost';
// $u = 'kaporchu_kc';
$u = 'root';
// $p = 'k@porchuporp@ss1234';
$p = '';
Cartalyst\Sentry\Facades\Native\Sentry::setupDatabaseResolver( new PDO($dsn, $u, $p) );

require 'cors_header.php';

session_start();
$header_location = 'http://kaporchupor.com/plexpos';
$useractivationemail;

require 'flight/Flight.php';

require 'lib/EasyPDO.php';
//Flight::register('db', 'EasyPDO', array('mysql:dbname=kaporchu_kc;host=localhost;charset=UTF8', "kaporchu_kc", "k@porchuporp@ss1234"));
Flight::register('db', 'EasyPDO', array('mysql:dbname=kaporchu_kc;host=localhost;charset=UTF8', "root", ""));

Flight::route('/checkLogin', function(){
    if ( ! Cartalyst\Sentry\Facades\Native\Sentry::check()){
        $output = array( 'status' => false );
        Flight::json($output);
    }
    else{
        $currentUser = Cartalyst\Sentry\Facades\Native\Sentry::getLogin();
        $currentUserDetails = Cartalyst\Sentry\Facades\Native\Sentry::findUserByLogin($currentUser);
        Flight::json(array(
            "status"=> true,
            "id"=> $currentUserDetails['id'],
            "first_name"=> $currentUserDetails['first_name'],
            "last_name"=> $currentUserDetails['last_name'],
            "last_login"=> $currentUserDetails['last_login'],
        ));
    }
});

Flight::map('checkLogin', function(){
    if ( ! Cartalyst\Sentry\Facades\Native\Sentry::check()){
        // User is not logged in, or is not activated
        $output = array(
            'status' => 'login'
        );
        Flight::json($output);
    }
    else{
        // User is logged in
    }
});

Flight::route('/', function(){
    echo getcounter(2);
});

Flight::route('/docroot', function(){
    echo $_SERVER['HTTP_HOST'];
});

Flight::route('POST /login', function(){
    $entityBody = file_get_contents('php://input');
    $data = json_decode($entityBody);

    try {
        // validate input
        $email = filter_var($data->email, FILTER_SANITIZE_EMAIL);
        $password = strip_tags(trim($data->password));

        // set login credentials
        $credentials = array(
          'email'    => $email,
          'password' => $password,
        );
        // authenticate
        $currentUser = Cartalyst\Sentry\Facades\Native\Sentry::authenticate($credentials, false);
        $user = Cartalyst\Sentry\Facades\Native\Sentry::findUserByLogin($currentUser->getLogin());

        Cartalyst\Sentry\Facades\Native\Sentry::login($user, false);

        Flight::json(array(
            'status'  => 'success',
            'message' => 'Logged in as ' . $currentUser->getLogin(),
            'token' => md5(md5($email))
        ));
    } catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'Login field is required'
        ));
    }
    catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'Password field is required.'
        ));
    }
    catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'Wrong password, try again.'
        ));
    }
    catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User was not found.'
        ));
    }
    catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User is not activated.'
        ));
    }
    catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User is suspended.'
        ));
    }
    catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User is banned.'
        ));
    }
});

Flight::route('/user/new', function(){
    try {

        $user = Cartalyst\Sentry\Facades\Native\Sentry::createUser(array(
            'email'    => 'partha@kiteplexit.com',
            'password' => '123456',
            'first_name' => 'partha',
            'last_name' => 'dhar',
            'activated' => true,
        ));

    } catch (Exception $e) {
        echo $e->getMessage();
    }
});

Flight::route('POST /plexcodeLogin', function(){
    $entityBody = file_get_contents('php://input');
    $data = json_decode($entityBody);
    $db = Flight::db();
    $username = $data->username;
    $password = md5(md5($data->password));

    $user_info = $db->fetchAll("SELECT id, name FROM users WHERE name=:name AND password=:password",
        array('name'=>$username,'password'=>$password)
    );
    if($user_info) {
        $_SESSION['plexcode'] = 'YES';
        $_SESSION['plexcodetoken'] = md5(md5($username));

        Flight::json(array(
            'status'  => 'success',
            'token' => md5(md5($username))
        ));
    } else {
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Invalid login or password'
        ));
    }
});
Flight::route("/plexcodedata", function(){
    $db = Flight::db();

    $rows = $db->fetchAll("SELECT id, style_no, category, color, size, upc_ean_isbn, selling_price, quantity FROM items");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array(
                    "id"=> $row['id'],
                    "style_no"=> $row['style_no'],
                    "category"=> $row['category'],
                    "color"=> $row['color'],
                    "size"=> $row['size'],
                    "upc_ean_isbn"=> $row['upc_ean_isbn'],
                    "selling_price"=> $row['selling_price'],
                    "quantity"=> $row['quantity']
                );
    }

    Flight::json($tmp);
});
Flight::route("/plexcodeLogout", function(){
    $_SESSION['plexcode'] = '';
    $_SESSION['plexcodetoken'] = '';
    unset($_SESSION['plexcode']);
    unset($_SESSION['plexcodetoken']);
    $output = array(
        'status' => 'success'
    );
    Flight::json($output);
});

//// CHART FOR DASHBOARD start

Flight::route('GET /salesHistry', function(){
    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT total, created_at FROM sales ORDER BY created_at DESC");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("taka"=> $row['total'],"date"=> $row['created_at']);
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

Flight::route('GET /itemSaleHistory', function(){
    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT id, created_at, total_qty FROM sales");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("id"=> $row['id'],"created_at"=> $row['created_at'],"total_qty"=> $row['total_qty']);
    }
    Flight::json($tmp);
});

//// CHART FOR DASHBOARD stop

//// colors start
Flight::route('GET /allcolors', function(){
    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT * FROM colors");
    Flight::json($rows);
});

Flight::route('GET /colors', function(){
    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT DISTINCT id, color FROM items GROUP BY color");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("name"=> $row['color'], "id"=> $row['id']); //$row['color'];
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

Flight::route('POST /colorsedit', function(){    
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);
    $db = Flight::db();
    foreach ($params as $row) {
        $data = array(
            'name'=>$row->name,
            'hashcode'=>$row->hashcode
        );
        $flg = $db->insert("colors",$data);
    }  
});

Flight::route('POST /colors', function(){

    $entityBody = file_get_contents('php://input');
    $data = json_decode($entityBody);

    $db = Flight::db();

    $category_id = $data->category_id;
    $style_no = $data->style_no;

    $rows = $db->fetchAll("SELECT * FROM items WHERE category_id=:category_id AND style_no=:style_no GROUP BY color",
        array('category_id'=>$category_id,'style_no'=>$style_no)
    );
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("name"=> $row['color'], "id"=> $row['id']); //$row['color'];
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

//// colors end

//// styles start

Flight::route('GET /styles', function(){
    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT DISTINCT id, style_no FROM items GROUP BY style_no");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("name"=> $row['style_no'], "id"=> $row['id']); //$row['color'];
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

Flight::route('GET /styles/@id', function($id){
    Flight::checkLogin();

    $db = Flight::db();

    $rows = $db->fetchAll("SELECT DISTINCT id, style_no FROM items WHERE category_id=:id GROUP BY style_no",array('id'=>$id));
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("name"=> $row['style_no'], "id"=> $row['id']); //$row['color'];
    }

    Flight::json($tmp);
});

//// styles end

//// size start
Flight::route('GET /sizes', function(){
    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT DISTINCT id, size FROM items GROUP BY size");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("name"=> $row['size'], "id"=> $row['id']); //$row['color'];
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

Flight::route('POST /sizes', function(){

    $entityBody = file_get_contents('php://input');
    $data = json_decode($entityBody);

    $db = Flight::db();

    $category_id = $data->category_id;
    $style_no = $data->style_no;
    $color = $data->color;

    $rows = $db->fetchAll("SELECT * FROM items WHERE category_id=:category_id AND style_no=:style_no AND color=:color GROUP BY size",
        array('category_id'=>$category_id,'style_no'=>$style_no,'color'=>$color)
    );
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("name"=> $row['size'], "id"=> $row['id']); //$row['color'];
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

//// size end

//// category start

Flight::route('GET /categoriesList', function(){
    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT id, name FROM categories");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array(
            "id"=> $row['id'],
            "name"=> $row['name'],
            "text"=> $row['name'],
            "value"=> $row['name']
        );
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

Flight::route('GET /categoriesName', function(){
    //Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchAll("SELECT name FROM categories");
    Flight::json($data);
});
Flight::route('GET /categories', function(){
    //Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchAll("SELECT * FROM categories ORDER BY parent_id,`position`");
    Flight::json($data);
});
Flight::route('GET /categories/@id/@rand', function($id, $rand){
    //Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM categories WHERE id=:id",array('id'=>$id));
    $data['docroot'] = $_SERVER['HTTP_HOST'];

    Flight::json($data);
});
Flight::route('POST /categories/hierarchy', function(){
//    Flight::checkLogin();

    $db = Flight::db();
    $entityBody = file_get_contents('php://input');
//    var_dump($entityBody); exit;
    $rows = json_decode($entityBody);

    foreach ($rows as &$row) {

        $data = array(
            'position'=>$row->position,
            'parent_id'=>$row->parent_id
        );
        $flg = $db->update("categories",$data, array('id'=>$row->id));
    }
    Flight::json(array(
        'status'  => 'success',
        'message' => 'School Saved Successfully.'
    ));
});

Flight::route('POST /categories', function(){
    Flight::checkLogin();
    $request = Flight::request();

    $itemName = $request->data['name'];
    $address = "/assets/images/category/";
    $category_image_store_path = $_SERVER['DOCUMENT_ROOT'].$address;


    $data = array(
        'name'=>$request->data['name']
    );
    if(isset($request->data['imageUrl'])){
	$imageuri = $request->data['imageUrl'];
	$data['imageUrl'] = $imageuri;

	if (!file_exists($category_image_store_path)) {
	        mkdir($category_image_store_path, 0777, true);
	    }

	if(trim($request->files['image_video']['name'])){
	        $fileName =$request->files['image_video']['name'];

	        $temporaryName = $request->files['image_video']['tmp_name'];
	        move_uploaded_file($temporaryName, $category_image_store_path . $itemName . $fileName);
	        $data['imageUrl'] = $address.$itemName.$request->files['image_video']['name'];
	    }
	}

    $db = Flight::db();

    if( isset($request->data['id']) && trim($request->data['id']) ){
        $id = $request->data['id'];
        $data['updated_at'] = date("Y-m-d H:i:s");
        $flg = $db->update("categories",$data, array('id'=>$id));
    }else{
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['parent_id']= '-1';
        $data['position']= '0';
        $flg = $db->insert("categories",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Category Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Category Save Error.'
        ));
    }
});
// Flight::route('POST /categories', function(){
// //    Flight::checkLogin();
//     $entityBody = file_get_contents('php://input');
//     $params = json_decode($entityBody);

//     $db = Flight::db();
//     $data =         array(
//         'name'=>$params->name
//     );

//     if(isset($params->id)){
//         $flg = $db->update("categories",$data, array('id'=>$params->id));
//     }else{
//         $data['parent_id']= '-1';
//         $data['position']= '0';
//         $flg = $db->insert("categories",$data);
//     }

//     if($flg){
//         Flight::json(array(
//             'status'  => 'success',
//             'message' => 'Menu Saved Successfully.'
//         ));
//     }else{
//         Flight::json(array(
//             'status'  => 'failure',
//             'message' => 'Error Occurred.'
//         ));
//     }
// });
Flight::route('DELETE /categories/@id', function($id){

    $db = Flight::db();
    $flg = $db->delete("categories", array('id'=>$id));
    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Menu deleted successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Menu could not be deleted.'
        ));
    }
});

//// categories end

//// menu start

Flight::route('GET /menuList', function(){
    Flight::checkLogin();
    $db = Flight::db();
    $rows = $db->fetchAll("SELECT id, menu_name FROM menus");
    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array(
            "id"=> $row['id'],
            "menu_name"=> $row['menu_name']
        );
    }
    //var_dump($tmp); exit;
    Flight::json($tmp);
});

Flight::route('GET /getMainMenu/@id/@rand', function($id, $rand){
    Flight::checkLogin();
    $db = Flight::db();
    $data = $db->fetchRow("SELECT * FROM menus WHERE id=:id",array('id'=>$id));
    Flight::json($data);
});

Flight::route('POST /saveMenu', function(){
    Flight::checkLogin();
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);

    $db = Flight::db();

    $mp = isset($params->menu_position)?$params->menu_position:null;
    $data = array(
        'menu_name'=>$params->menu_name,
        'menu_position'=>$mp
    );

    if(isset($params->id)){
        $data['updated_at'] = date("Y-m-d H:i:s");
        try {
            $flg = $db->update("menus",$data, array('id'=>$params->id));
        } catch (Exception $e) {
            Flight::json(array(
                'status'  => 'error',
                'message' => $e
            ));
        }
    }else{
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $flg = $db->insert("menus",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Menu Created and Saved Successfully.',
            'menu_id' => ''
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Error Occurred.'
        ));
    }
});

Flight::route('GET /menuItems/@id/@rand', function($id, $rand){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchAll("SELECT * FROM menu WHERE menu_id=:id ORDER BY parent_id,`position`",array('id'=>$id));
    Flight::json($data);
});
Flight::route('GET /menus/@id', function($id){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM menu WHERE id=:id",array('id'=>$id));

    Flight::json($data);
});
Flight::route('POST /menus/hierarchy', function(){
//    Flight::checkLogin();

    $db = Flight::db();
    $entityBody = file_get_contents('php://input');
//    var_dump($entityBody); exit;
    $rows = json_decode($entityBody);

    foreach ($rows as &$row) {

        $data = array(
            'position'=>$row->position,
            'parent_id'=>$row->parent_id
        );
        $flg = $db->update("menu",$data, array('id'=>$row->id));
    }
    Flight::json(array(
        'status'  => 'success',
        'message' => 'School Saved Successfully.'
    ));
});

Flight::route('POST /categorySaveMenu', function(){
    Flight::checkLogin();
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);

    $db = Flight::db();
    foreach ($params->datapack as $row) {
        $data = array(
            'menu_id'=>$row->menu_id,
            'name'=>$row->name,
            'is_leaf'=>$row->is_leaf,
            'type_id'=>$row->type_id,
            'uri'=>$row->uri,
            'parent_id'=>$row->parent_id, 
            'position'=>$row->position,
            'created_at'=>date("Y-m-d H:i:s")
        );
        $flg = $db->insert("menu",$data);
    }
    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'count'   => count($params->datapack),
            'message' => 'category Saved into Menu Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Menu Saving as category error.'
        ));
    }
    // Flight::json($params->datapack);
});

Flight::route('POST /menus', function(){
    Flight::checkLogin();
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);

    $db = Flight::db();
    $data = array(
        'menu_id'=>$params->menu_id,
        'name'=>$params->name,
        'is_leaf'=>$params->is_leaf,
        'type_id'=>$params->type_id,
        'uri'=>$params->uri
    );

    if(isset($params->id)){
        $flg = $db->update("menu",$data, array('id'=>$params->id));
    }else{
        $data['parent_id']= '-1';
        $data['position']= '0';
        $flg = $db->insert("menu",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Menu Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Error Occurred.'
        ));
    }

});
Flight::route('DELETE /menus/@id', function($id){

    $db = Flight::db();
    $flg = $db->delete("menu", array('id'=>$id));
    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Menu deleted successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Menu could not be deleted.'
        ));
    }
});
Flight::route('DELETE /mainMenus/@id', function($id){

    $db = Flight::db();
    $flg = $db->delete("menus", array('id'=>$id));
    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Menu deleted successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Menu could not be deleted.'
        ));
    }
});

//// menus end

//// WIDGET START
Flight::route('GET /widgets/@position/@rand', function($position,$rand){
    Flight::checkLogin();
    $db = Flight::db();
    try{
        $rows = $db->fetchAll("SELECT * FROM widget WHERE position=:position",array('position'=>$position));
        $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        $data = array(
            'rows'=>$rows?$rows:[],
            'total'=>$total['total']
        );
        $data['docroot'] = $_SERVER['HTTP_HOST'];
        Flight::json($data);
    } catch (Exception $e) {
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'No Data in Database'
        ));
    }
});

Flight::route('POST /widget/@pos', function($pos){
    Flight::checkLogin();
    $request = Flight::request();

    switch ($pos) {
        case '1':
            $address = "/assets/images/widgets/new_arrivals/";
            break;
        case '2':
            $address = "/assets/images/widgets/womens/";
            break;
        case '3':
            $address = "/assets/images/widgets/mens/";
            break;
        case '4':
            $address = "/assets/images/widgets/kids/";
            break;
        case '5':
            $address = "/assets/images/widgets/accessories/";
            break;
        
        default:
            
            break;
    }

    $itemName = $request->data['title'];
    $product_image_store_path = $_SERVER['DOCUMENT_ROOT'].$address;

    $data = array(
        'title'=>$request->data['title'],
        'description'=>$request->data['description'],
        'uri_name'=>$request->data['uri_name']
    );

    if (!file_exists($product_image_store_path)) {
        mkdir($product_image_store_path, 0777, true);
    }

    if(trim($request->files['image_video']['name'])){
        $fileName =$request->files['image_video']['name'];

        $temporaryName = $request->files['image_video']['tmp_name'];
        move_uploaded_file($temporaryName, $product_image_store_path."/".$fileName);
        $data['avatar'] = $address.$request->files['image_video']['name'];
    }

    $db = Flight::db();

    if( isset($request->data['id']) && trim($request->data['id']) ){
        $id = $request->data['id'];
        $data['createdAt'] = date("Y-m-d H:i:s");
        $flg = $db->update("widget",$data, array('position'=>$pos));
    }else{
        $data['createdAt'] = date("Y-m-d H:i:s");
        // $data['updated_at'] = date("Y-m-d H:i:s");
        $flg = $db->insert("widget",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Widget Saved Successfully'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Widget Save Error Occurred'
        ));
    }
});

//// WIDGET ENDS

////Logo start
Flight::route('GET /logo/@rand', function($rand){
    Flight::checkLogin();
    $db = Flight::db();
    $rows = $db->fetchAll("SELECT * FROM logo");    
    $data = array(
        'rows'=>$rows?$rows:[],
        'docroot' =>$_SERVER['HTTP_HOST']
    );
    Flight::json($data);
});

Flight::route('POST /savelogo', function(){
    Flight::checkLogin();
    $request = Flight::request();

    $address = "/assets/images/logo/";
    $itemName = $request->data['title'];
    $product_image_store_path = $_SERVER['DOCUMENT_ROOT'].$address;

    $data = array(
        'baseline'=>$request->data['title']
    );

    if (!file_exists($product_image_store_path)) {
        mkdir($product_image_store_path, 0777, true);
    }

    if(trim($request->files['image_video']['name'])){
        $fileName =$request->files['image_video']['name'];

        $temporaryName = $request->files['image_video']['tmp_name'];
        move_uploaded_file($temporaryName, $product_image_store_path."/".$fileName);
        $data['avatar'] = $address.$request->files['image_video']['name'];
    }

    $db = Flight::db();
    $data['created_at'] = date("Y-m-d H:i:s");
    $flg = $db->update("logo",$data, array('id'=>$request->data['id']));

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Logo Saved Successfully'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Logo Save Error Occurred'
        ));
    }
});
////Logo end

//// category_discounts start

Flight::route('POST /discount_categories', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $db = Flight::db();

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM category_discounts LIMIT ".$off.",".$lmt);

    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(

        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

Flight::route('GET /discount_category/@id/@rand', function($id,$rand){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM category_discounts WHERE id=:id",array('id'=>$id));

    Flight::json($data);
});

Flight::route('POST /discount_category', function(){
    Flight::checkLogin();
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);

    $db = Flight::db();
    $data =         array(
        'type'=>$params->type,
        'percent'=>$params->percent
    );

    if(isset($params->id)){
        $flg = $db->update("category_discounts",$data, array('id'=>$params->id));
    }else{
        $flg = $db->insert("category_discounts",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Discount Category Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Error Occurred.'
        ));
    }
});
//// category_discounts end

//// CASH REGISTER START

Flight::route("POST /cashRegisterList", function(){
    Flight::checkLogin();

    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $db = Flight::db();

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    if (isset($params['filter'])) {
        $eventFilter = $params['filter'];
        if (!$params['filter'] == '' || $params['filter'] == '0') {
            $rows = $db->fetchAll("SELECT 
                cash_register_status.last_balance, 
                cash_register_status.balance, 
                cash_register_status.event, 
                cash_registers.till_no, 
                outlets.name, 
                cash_register_log.cash_register_id, 
                cash_register_log.id, 
                cash_register_log.event, 
                cash_register_log.business_day, 
                cash_register_log.remarks, 
                cash_register_log.amount 
                FROM cash_register_log 
                JOIN cash_registers ON cash_register_log.cash_register_id = cash_registers.id 
                JOIN outlets ON cash_registers.outlet_id = outlets.id 
                JOIN cash_register_status ON cash_register_status.cash_register_id = cash_register_log.cash_register_id 
                WHERE cash_register_log.event = ".$params['filter']." 
                ORDER BY cash_register_log.id DESC");
            $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        } else {
            $rows = $db->fetchAll("SELECT 
                cash_register_status.last_balance, 
                cash_register_status.balance, 
                cash_register_status.event, 
                cash_registers.till_no, 
                outlets.name, 
                cash_register_log.cash_register_id, 
                cash_register_log.id, 
                cash_register_log.event, 
                cash_register_log.business_day, 
                cash_register_log.remarks, 
                cash_register_log.amount 
                FROM cash_register_log 
                JOIN cash_registers ON cash_register_log.cash_register_id = cash_registers.id 
                JOIN outlets ON cash_registers.outlet_id = outlets.id 
                JOIN cash_register_status ON cash_register_status.cash_register_id = cash_register_log.cash_register_id 
                ORDER BY cash_register_log.id DESC");
            $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        }

    } else {
        $rows = $db->fetchAll("SELECT 
            cash_register_status.last_balance, 
            cash_register_status.balance, 
            cash_register_status.event, 
            cash_registers.till_no, 
            outlets.name, 
            cash_register_log.cash_register_id, 
            cash_register_log.id, 
            cash_register_log.event, 
            cash_register_log.business_day, 
            cash_register_log.remarks, 
            cash_register_log.amount 
            FROM cash_register_log 
            JOIN cash_registers ON cash_register_log.cash_register_id = cash_registers.id 
            JOIN outlets ON cash_registers.outlet_id = outlets.id 
            JOIN cash_register_status ON cash_register_status.cash_register_id = cash_register_log.cash_register_id 
            ORDER BY cash_register_log.id DESC");
        $total = $db->fetchRow("SELECT FOUND_ROWS() total");

    }
    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

//// CASH REGISTER END

//// products start

Flight::route('GET /getItems', function(){
    Flight::checkLogin();
    $db = Flight::db();
    $rows = $db->fetchAll("SELECT id, item_name FROM items GROUP BY item_name");
    $tmp = [];
    foreach ($rows as $row) {
        $tmp[] = array(
            "id"=> $row['id'],
            "name"=> $row['item_name']
        );
    }

    Flight::json($tmp);
});

Flight::route('POST /productslistfordis', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $db = Flight::db();
    $query;

    if (isset($params['item_id'])) {
        // $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items WHERE id='".$params['item_id']."' ORDER BY id DESC LIMIT ".$off.",".$lmt);
        $rows = $db->fetchAll("SELECT * FROM items WHERE id='".$params['item_id']."' ORDER BY id DESC");
        // "SELECT SQL_CALC_FOUND_ROWS * FROM employees WHERE department='sales' LIMIT 0,10"
        // $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        // $data = array(
        //     'rows'=>$rows?$rows:[],
        //     'total'=>$total['total']
        // );

    } else if(isset($params['category_id'])) {
        // $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items WHERE category_id='".$params['category_id']."' ORDER BY id DESC LIMIT ".$off.",".$lmt);
        $rows = $db->fetchAll("SELECT * FROM items WHERE category_id='".$params['category_id']."' ORDER BY id DESC");
        // "SELECT SQL_CALC_FOUND_ROWS * FROM employees WHERE department='sales' LIMIT 0,10"
        // $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        // $data = array(
        //     'rows'=>$rows?$rows:[],
        //     'total'=>$total['total']
        // );
    } else if( isset($params['search']) ) {
        $srh = $params['search'];
        if($srh == '') {
            // $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items LIMIT ".$off.",".$lmt);
            $rows = $db->fetchAll("SELECT * FROM items");
            // $total = $db->fetchRow("SELECT FOUND_ROWS() total");
            // $data = array(
            //     'rows'=>$rows?$rows:[],
            //     'total'=>$total['total']
            // );
        } else {
            // $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items WHERE category = '".$srh."' ORDER BY id DESC LIMIT ".$off.",".$lmt);
            $rows = $db->fetchAll("SELECT * FROM items WHERE category = '".$srh."' ORDER BY id DESC");
            // $total = $db->fetchRow("SELECT FOUND_ROWS() total");
            // $data = array(
            //     'rows'=>$rows?$rows:[],
            //     'total'=>$total['total']
            // );
        }
    } else {
        // $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items ORDER BY id DESC LIMIT ".$off.",".$lmt);
        $rows = $db->fetchAll("SELECT * FROM items ORDER BY id DESC");
        // $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        // $data = array(
        //     'rows'=>$rows?$rows:[],
        //     'total'=>$total['total']
        // );
    }

    Flight::json($rows?$rows:[]);
});

Flight::route('POST /products', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $db = Flight::db();
    $query;

    if (isset($params['item_id'])) {
        $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items WHERE id='".$params['item_id']."' ORDER BY id DESC LIMIT ".$off.",".$lmt);
        // "SELECT SQL_CALC_FOUND_ROWS * FROM employees WHERE department='sales' LIMIT 0,10"
        $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        $data = array(
            'rows'=>$rows?$rows:[],
            'total'=>$total['total']
        );

    } else if(isset($params['category_id'])) {
        $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items WHERE category_id='".$params['category_id']."' ORDER BY id DESC LIMIT ".$off.",".$lmt);
        // "SELECT SQL_CALC_FOUND_ROWS * FROM employees WHERE department='sales' LIMIT 0,10"
        $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        $data = array(
            'rows'=>$rows?$rows:[],
            'total'=>$total['total']
        );
    } else if( isset($params['search']) ) {
        $srh = $params['search'];
        if($srh == '') {
            $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items LIMIT ".$off.",".$lmt);
            $total = $db->fetchRow("SELECT FOUND_ROWS() total");
            $data = array(
                'rows'=>$rows?$rows:[],
                'total'=>$total['total']
            );
        } else {
            $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items WHERE category = '".$srh."' ORDER BY id DESC LIMIT ".$off.",".$lmt);
            $total = $db->fetchRow("SELECT FOUND_ROWS() total");
            $data = array(
                'rows'=>$rows?$rows:[],
                'total'=>$total['total']
            );
        }
    } else {
        $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM items ORDER BY id DESC LIMIT ".$off.",".$lmt);
        $total = $db->fetchRow("SELECT FOUND_ROWS() total");
        $data = array(
            'rows'=>$rows?$rows:[],
            'total'=>$total['total']
        );
    }

    Flight::json($data);
});

Flight::route('GET /product/@id/@rand', function($id, $rand){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM items WHERE id=:id",array('id'=>$id));
    $data['docroot'] = $_SERVER['HTTP_HOST'];

    Flight::json($data);
});

Flight::route('POST /product', function(){
    Flight::checkLogin();
    $request = Flight::request();
    // $entityBody = file_get_contents('php://input');
    // $params = json_decode($entityBody);

    $category_name = $request->data['category'];
    $category_id = GetCatIdbyName($category_name);
    // $address = "/plexpos/archive/products/$category_name/";
    // $path = $_SERVER['DOCUMENT_ROOT'].$address;
    $address = "/assets/images/";
    $itemName = $request->data['category'].'-'.$request->data['color'].'-'.$request->data['size'];
    $product_image_store_path = $_SERVER['DOCUMENT_ROOT'].$address;

    $data = array(
//        'category'=>$request->data['category'],
//        'color'=>$request->data['color'],
//        'size'=>$request->data['size'],
        'style_no'=>$request->data['style_no'],
        // 'quantity'=>$request->data['quantity'],
        'category_id'=>$category_id,
        'discount'=>$request->data['discount'],
        'description'=>$request->data['description'],
        'product_name'=>$request->data['product_name'],
        'new_arrivals'=>$request->data['new_arrivals']?1:0,
        'special_offer'=>$request->data['special_offer']?1:0,
        'item_name'=>$itemName,
        'selling_price'=>$request->data['selling_price']
    );

    $db = Flight::db();

    if( isset($request->data['new_category']) && trim($request->data['new_category']) ){
        $data['category'] = $request->data['new_category'];
    }
    if (isset($request->data['category']) && trim($request->data['category']) ){
        $data['category'] = $request->data['category'];
    }
    if( isset($request->data['new_color']) && trim($request->data['new_color']) ){
        $data['color'] = $request->data['new_color'];
    }
    if (isset($request->data['color']) && trim($request->data['color']) ){
        $data['color'] = $request->data['color'];
    }
    if( isset($request->data['new_size']) && trim($request->data['new_size']) ){
        $data['size'] = $request->data['new_size'];
    }
    if (isset($request->data['size']) && trim($request->data['size']) ){
        $data['size'] = $request->data['size'];
    }

    if(isset($request->data['create_color']) && trim($request->data['create_color'])){
        $color_code = preg_replace('/\s+/', '', strtolower($request->data['name_new_color']) );
        $newcolordata = array(
            'hashcode'=>$request->data['create_color'],
            'name'=>$request->data['name_new_color'],
            'code'=>$color_code
        );
        $colflg = $db->insert("colors",$newcolordata);
        $data['color'] = $request->data['name_new_color'];
    }

    // if (!file_exists($path)) {
    //     mkdir($path, 0777, true);
    // }

    if(trim($request->files['image_video']['name'])){
        $fileName =$request->files['image_video']['name'];

        $temporaryName = $request->files['image_video']['tmp_name'];
        move_uploaded_file($temporaryName, $product_image_store_path . $itemName . $fileName);
        $data['avatar'] = $address.$itemName.$request->files['image_video']['name'];
    } else {
        if( isset($request->data['id']) && trim($request->data['id']) ){
            $imageChkData = checkImage($request->data['id']);
            if($imageChkData == "") {
                $fileName = 'no_image.png';
                $data['avatar'] = $address.$fileName;
            } else {
                $fileName = $imageChkData;
                $data['avatar'] = $fileName;
            }
        }
    }

    if( isset($request->data['id']) && trim($request->data['id']) ){
        $id = $request->data['id'];
        $data['updated_at'] = date("Y-m-d H:i:s");
        $flg = $db->update("items",$data, array('id'=>$id));
    }else{
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['upc_ean_isbn'] = NextSerial(); // auto increament
        $flg = $db->insert("items",$data);
    }

    var_dump($data);

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Product Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Error Occurred.'
        ));
    }
});
function NextSerial(){
    $db = Flight::db();

    $data = $db->fetchOne("SELECT  MAX(upc_ean_isbn) + 1 AS 'next_serial' FROM items");
    //var_dump($data);exit();
    return $data;
}
function GetCatIdbyName($name){
    $db = Flight::db();

    $data = $db->fetchOne("SELECT id FROM categories where name=:name", array('name'=>$name));
    //var_dump($data);exit();
    return $data;
}
function checkImage($id){
    $db = Flight::db();
    $data = $db->fetchOne("SELECT avatar FROM items where id=:id", array('id'=>$id));
    return $data;
}

Flight::route('GET /productDelete/@id', function($id){
    $db = Flight::db();
    $flg = $db->delete('items', array('id'=>$id));

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Product deleted successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Product could not be deleted.'
        ));
    }
});

Flight::route('POST /product_add_in__stock', function(){
    Flight::checkLogin();
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);

    $db = Flight::db();

    $data =  array(
        'quantity'=> intval($params->total_quantity)
    );
    $tdata = array(
        'item_id'=>$params->id,
        'in_out_qty'=>$params->total_quantity,
    );

    if(isset($params->id)){

        $flg = $db->insert("inventories",$tdata);
        $flg = $db->update("items",$data, array('id'=>$params->id));
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Product Updated Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Error Occurred.'
        ));
    }
});

Flight::route("POST /itemAvatar", function(){
    Flight::checkLogin();

    $ds = DIRECTORY_SEPARATOR;
    $storeFolder = 'uploads';

    if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        echo $tempFile;
        // $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;
        // $targetFile =  $targetPath. $_FILES['file']['name'];
        // move_uploaded_file($tempFile,$targetFile);
    }
});
//// categories end
//// sales start

Flight::route('POST /sales', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;
    // $params = Flight::request()->data;

    $db = Flight::db();

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM sales ORDER BY id DESC LIMIT ".$off.",".$lmt);
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(

        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

//// Sales end

//// Online Delivery start

Flight::route('GET /deliveryStatus/@status', function($status){
//    Flight::checkLogin();

    $db = Flight::db();
    $rows = $db->fetchAll("SELECT id, 
                                  customer_name, 
                                  delivery_address, 
                                  delivery_email, 
                                  created_at  
                                  FROM online_deliveries 
                                  WHERE delivery_status=:status 
                                  ORDER BY id DESC",
                                  array('status'=>$status)
                         );
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );

    Flight::json($data);
});

Flight::route('POST /deliveryStatus', function(){
    Flight::checkLogin();
    $db = Flight::db();
    $rows = $db->fetchAll("SELECT online_deliveries.id, 
                                  online_deliveries.customer_name, 
                                  online_deliveries.delivery_status, 
                                  online_deliveries.delivery_address, 
                                  online_deliveries.delivery_email, 
                                  online_deliveries.delivery_phonenumber, 
                                  online_deliveries.created_at, 
                                  online_deliveries.updated_at, 
                                  sales.total_qty, 
                                  sales.total_discount_id, 
                                  sales.net_total, 
                                  sales.total, 
                                  sales.invoice_number  
                                  FROM online_deliveries 
                                  JOIN sales ON online_deliveries.sale_id = sales.id"
                         );
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );

    Flight::json($data);
});

Flight::route('GET /delivery/@id/@rand', function($id, $rand){
    Flight::checkLogin();
    $db = Flight::db();
    $data = $db->fetchRow("SELECT 
                          online_deliveries.id, 
                          online_deliveries.customer_name, 
                          online_deliveries.delivery_status, 
                          online_deliveries.delivery_address, 
                          online_deliveries.delivery_email, 
                          online_deliveries.delivery_phonenumber, 
                          online_deliveries.created_at, 
                          online_deliveries.updated_at, 
                          sales.total_qty, 
                          sales.total_discount_id, 
                          sales.net_total, 
                          sales.total, 
                          sales.invoice_number  
                          FROM online_deliveries 
                          JOIN sales ON online_deliveries.sale_id = sales.id 
                          WHERE online_deliveries.id=:id" , array('id'=>$id));
    Flight::json($data);
});

Flight::route('POST /online_order_details', function(){
    Flight::checkLogin();
    $request = Flight::request();

    $delivery_status = $request->data['status_change'];

    if($delivery_status == '1'){

    }


    $data = array(
        'delivery_status'=>$request->data['status_change']
    );
    $db = Flight::db();

    if(isset($request->data['id']) && trim($request->data['id'])){
        $flg = $db->update("online_deliveries",$data, array('id'=>$request->data['id']));
    }else{
        $flg = $db->insert("online_deliveries",$data);
    }
    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'online orders altered Successfully.'
        ));
    } else {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'online orders alter error.'
        ));
    }
});

//// Online Delivery end

////INVENTORY START

Flight::route('POST /inventory', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $db = Flight::db();

    //THIS QUERY ONLY PULLS THE VALID EXISTING ROWS IN ITEMS, INVENTORIES AND USER TABLES
    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS inventories.id, inventories.in_out_qty, inventories.remarks, inventories.created_at,inventories.updated_at, items.item_name, users.first_name FROM inventories JOIN items ON inventories.item_id = items.id JOIN users ON inventories.user_id = users.id ORDER BY inventories.id DESC LIMIT ".$off.",".$lmt);
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

////INVENTORY END

////PAYMENT METHOD START

Flight::route('POST /paymentReport', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $db = Flight::db();

    //THIS QUERY ONLY PULLS THE VALID EXISTING ROWS IN sales_payment_methods, SALES AND payment_methods TABLES
    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS sales_payment_methods.id, sales.customer_name, sales.employee_name, sales.total_qty, sales.total_discount_id, sales.total, payment_methods.type, sales_payment_methods.created_at FROM sales_payment_methods JOIN sales ON sales_payment_methods.sales_id = sales.id JOIN payment_methods ON sales_payment_methods.payment_methods_id = payment_methods.id ORDER BY sales.id DESC LIMIT ".$off.",".$lmt);
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

////PAYMENT METHOD END

Flight::route('/gallery/@category_id', function($category_id){
    //Flight::checkLogin();

    $params = Flight::request()->data;

    $db = Flight::db();
    $data = $db->fetchAll("SELECT * FROM archive WHERE category_id=:category_id ORDER BY position",
        array('category_id'=>$category_id)
    );
//    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
//    var_dump($total['total']); exit;
//    var_dump($rows);

    Flight::json($data);
});



Flight::route('GET /archive/@id', function($id){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM archive WHERE id=:id" , array('id'=>$id));
//    Flight::json($districts);
    Flight::json($data);
});

Flight::route('POST /archive', function(){
    Flight::checkLogin();
    $request = Flight::request();

//    var_dump($request); exit;
    // folder name localhost/archive/category_id/<files>
    $category_id = $request->data['category_id'];
    $path = $_SERVER['DOCUMENT_ROOT']."/plexpos/archive/$category_id/";
    $data = array(
        'category_id'=>$request->data['category_id'],
        'caption_bn'=>$request->data['caption_bn'],
        'caption_en'=>$request->data['caption_en'],
        'position'=>$request->data['position'],
//        'created'=> date("Y-m-d H:i:s")
    );
//    echo $path."/ebrarchive/archive/$category_id/";
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    if(trim($request->files['file_bn']['name'])){
        $fileName =$request->files['file_bn']['name'];

        $temporaryName = $request->files['file_bn']['tmp_name'];
        move_uploaded_file($temporaryName, $path . $fileName);
        $data['file_bn'] = $request->files['file_bn']['name'];
    }
    if(trim($request->files['file_vid_bn']['name'])){
        $fileName =$request->files['file_vid_bn']['name'];

        $temporaryName = $request->files['file_vid_bn']['tmp_name'];
        move_uploaded_file($temporaryName, $path . $fileName);
        $data['file_vid_bn'] = $request->files['file_vid_bn']['name'];
    }
    if(trim($request->files['file_en']['name'])){
        $fileName =$request->files['file_en']['name'];

        $temporaryName = $request->files['file_en']['tmp_name'];
        move_uploaded_file($temporaryName, $path . $fileName);
        $data['file_en'] = $request->files['file_en']['name'];
    }
    if(trim($request->files['file_vid_en']['name'])){
        $fileName =$request->files['file_vid_en']['name'];

        $temporaryName = $request->files['file_vid_en']['tmp_name'];
        move_uploaded_file($temporaryName, $path . $fileName);
        $data['file_vid_en'] = $request->files['file_vid_en']['name'];
    }
    if(trim($request->files['image_video']['name'])){
        $fileName =$request->files['image_video']['name'];

        $temporaryName = $request->files['image_video']['tmp_name'];
        move_uploaded_file($temporaryName, $path . $fileName);
        $data['image_video'] = $request->files['image_video']['name'];
    }

    // then save data
    $db = Flight::db();


    if(isset($request->data['id']) && trim($request->data['id'])){

        $flg = $db->update("archive",$data, array('id'=>$request->data['id']));
    }else{
//        var_dump($data);
        $flg = $db->insert("archive",$data);
    }
    Flight::json(array(
        'status'  => 'success',
        'message' => 'Archive Saved Successfully.'
    ));
});

Flight::route('DELETE /archive/@id/@type', function($id, $type){
    Flight::checkLogin();

    // $db = Flight::db();
    // $flg = $db->delete("archive", array('id'=>$id));
    // if($flg){
    //     Flight::json(array(
    //         'status'  => 'success',
    //         'message' => 'Archive deleted successfully.'
    //     ));
    // }else{
    //     Flight::json(array(
    //         'status'  => 'failure',
    //         'message' => 'Archive could not be deleted.'
    //     ));
    // }


    $target;
    switch ($type) {
        case "products":
            $target = 'items';
            break;
        case "categories":
            $target = 'categories';
            break;
        case "category_discounts":
            $target = 'category_discounts';
            break;
        case "customers":
            $target = 'customers';
            break;
        case 'employee':
            $target = 'employees';
            break;
        case 'pages':
            $target = 'pages';
            break;
        case'users':
            $target = 'users';
            break;
        case "sales":
            break;
        default:
            echo "Nothing to delete";
    }

    $db = Flight::db();
    $flg = $db->delete($target, array('id'=>$id));

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Archive deleted successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Archive could not be deleted.'
        ));
    }
});

// ....... OUTLET START .........
Flight::route('GET /outlet/', function(){
    Flight::checkLogin();

    $db = Flight::db();

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM outlets");

    $data = array(
        'rows'=>$rows?$rows:[]
    );
    Flight::json($data);
});

// ....... OUTLET END .........

// .....INVENTORY COUNTING START....

Flight::route('GET /fetchCounts/@params', function($params){
    Flight::checkLogin();

    $db = Flight::db();

    if($params == 'all') {
        $rows = $db->fetchAll("SELECT * FROM stock_counting ORDER BY created DESC");
    } else if($params == 'due'){
        $rows = $db->fetchAll("SELECT * FROM stock_counting WHERE status=1 ORDER BY created DESC");
    } else if ($params == 'open'){
        $rows = $db->fetchAll("SELECT * FROM stock_counting WHERE status=0 ORDER BY created DESC");
    } else if ($params == 'abandoned'){
        $rows = $db->fetchAll("SELECT * FROM stock_counting WHERE status=2 ORDER BY created DESC");
    } else if ($params == 'finished'){
        $rows = $db->fetchAll("SELECT * FROM stock_counting WHERE status=3 ORDER BY created DESC");
    } else if ($params == 'killed'){
        $rows = $db->fetchAll("SELECT * FROM stock_counting WHERE status=4 ORDER BY created DESC");
    }

    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array(
            "id"=> $row['id'],
            "count_name"=> $row['count_name'],
            "count_type"=> $row['count_type'],
            "outlet_warehouse_id"=> $row['outlet_warehouse_id'],
            "count_start_date"=> $row['count_start_date'],
            "count_close_date"=> $row['count_close_date'],
            "last_count_date"=> $row['last_count_date'],
            "countedby"=> getcounter($row['countedby'])
        );
    }

    Flight::json($tmp);
});
function getcounter($id){
    $user = Cartalyst\Sentry\Facades\Native\Sentry::findUserById($id);
    return $user['first_name']." ".$user['last_name'];
}

Flight::route('POST /initiateCount', function(){
    //Flight::checkLogin();

    $inventoryCount = file_get_contents('php://input');
    $params = json_decode($inventoryCount);

    $db = Flight::db();
    $data = array(
        'count_name'=>$params->count_name,
        'outlet_warehouse_id'=>$params->selectedOutletId,
        'count_start_date'=>$params->count_start_date,
        'created'=> date("Y-m-d H:i:s"),
        'status'=>0,    //0: OPEN
        // 'countedby'=> $_SESSION['user_info'][0]['id']
        'countedby'=> getUser()
    );

    $flg = $db->insert("stock_counting",$data);

    if($flg){
        $lastId = $db->fetchAll("SELECT LAST_INSERT_ID()");

        Flight::json(array(
            'status'  => 'success',
            'message' => 'Counting Started Successfully.',
            'countID' => $lastId[0]['LAST_INSERT_ID()']
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Counting Start Failed.'
        ));
    }
});
function getUser(){
    if ( ! Cartalyst\Sentry\Facades\Native\Sentry::check()){
        return false;
    }
    else{
        $currentUser = Cartalyst\Sentry\Facades\Native\Sentry::getLogin();
        $currentUserDetails = Cartalyst\Sentry\Facades\Native\Sentry::findUserByLogin($currentUser);
        return $currentUserDetails['id'];
    }
};

Flight::route('POST /countStatus', function(){
    Flight::checkLogin();
    $postData = file_get_contents('php://input');
    $params = json_decode($postData);

    $db = Flight::db();

    $taskflg = $params->taskflg;

    if ($taskflg == 'p') {
        $data = array('status'=>1);
    } else if($taskflg == 'r'){
        $data = array('status'=>0);
    } else if($taskflg == 'a'){
        $data = array('status'=>2);
    } else if($taskflg == 'comp') {
        $data = array('status'=>3, 'count_close_date'=>date("Y-m-d H:i:s"));
    } else if($taskflg == 'kill') {
        $data = array('status'=>4, 'count_close_date'=>date("Y-m-d H:i:s"));
    }

    $db = Flight::db();
    $flg = $db->update("stock_counting",$data, array('id'=>$params->id));

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Status change Success'
        ));
    } else if($taskflg == 'kill'){
        Flight::json(array(
            'status'  => 'kill',
            'message' => 'Count Has Deleted Successfully'
        ));
    } else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Status change Error'
        ));
    }
});

Flight::route('GET /getInventoryCountData/@id', function($id){
    Flight::checkLogin();

    $db = Flight::db();

    $rows = $db->fetchAll("SELECT * FROM stock_counting_items WHERE stock_counting_id=:id" , array('id'=>$id));

    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array(
            "id"=> $row['id'],
            "item_name"=> $row['item_name'],
            "item_id"=> $row['item_id'],
            "stock_count"=> $row['stock_count'],
            "inventory_quantity"=> $row['inventory_quantity'],
            "difference"=> $row['difference']
        );
    }

    Flight::json($tmp);
});

Flight::route('POST /inventory_expected', function(){
    Flight::checkLogin();

    $post_data = file_get_contents('php://input');
    $data = json_decode($post_data);

    $category_id = $data->category_id;
    $style_no = $data->style_no;
    $color = $data->color;
    $size = $data->size;

    $db = Flight::db();

    $rows = $db->fetchAll("SELECT id, item_name, quantity FROM items WHERE category_id=:category_id AND style_no=:style_no AND color=:color AND size=:size",array('category_id'=>$category_id, 'style_no'=>$style_no, 'color'=>$color, 'size'=>$size));

    $tmp = [];
    foreach($rows as $row){
        $tmp[] = array("quantity"=> $row['quantity'],"item_name"=> $row['item_name'],"id"=> $row['id']);
    }

    Flight::json($tmp);
});

function checkInventory($s_c_id, $i_id){
    $db = Flight::db();

    $data = $db->fetchOne("SELECT id FROM stock_counting_items where stock_counting_id=:s_c_id AND item_id=:i_id", array('s_c_id'=>$s_c_id, 'i_id'=>$i_id));
    //var_dump($data);exit();
    return $data;
}
Flight::route('POST /inventoryAdd', function(){
    Flight::checkLogin();

    $post_data = file_get_contents('php://input');
    $params = json_decode($post_data);

    $data = array(
        'item_name'=>$params->item_name,
        'stock_counting_id'=>$params->stock_counting_id,
        'item_id'=>$params->item_id,
        'inventory_quantity'=>$params->inventory_quantity,
        'stock_count'=>$params->stock_count,
        'difference'=>$params->difference,
        'created'=>date("Y-m-d H:i:s")
    );
    $db = Flight::db();

    $checkData = checkInventory($params->stock_counting_id, $params->item_id);

    if($checkData){
        $flg = $db->update("stock_counting_items",$data, array('id'=>$checkData));
    }else{
        $flg = $db->insert("stock_counting_items",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Successfully updated / Added Counting data'
        ));
    } else {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'Counting data add failed'
        ));
    }
    // Flight::json(array(
    //     'status'  => 'success',
    //     'message' => $checkData
    // ));
});

Flight::route('GET /reviewCountAll/@id', function($id){
    Flight::checkLogin();

    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $db = Flight::db();

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS items.item_name, items.quantity, items.selling_price, stock_counting_items.stock_count, stock_counting_items.difference FROM items JOIN stock_counting_items ON items.id=stock_counting_items.item_id WHERE stock_counting_items.stock_counting_id=".$id." GROUP BY items.item_name LIMIT ".$off.",".$lmt);
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");

    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

// .....INVENTORY COUNTING END....

// ............CUSTOMERS START.....................

Flight::route("POST /customerList", function(){
    Flight::checkLogin();

    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $db = Flight::db();

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM customers ORDER BY id ASC LIMIT ".$off.",".$lmt);
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

Flight::route('GET /customerEdit/@id/@rand', function($id, $rand){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM customers WHERE id=:id",array('id'=>$id));

    Flight::json($data);
});

Flight::route("POST /customerEdit", function(){
    Flight::checkLogin();
    $request = Flight::request();

    $address = "/plexpos/archive/customer/";
    $path = $_SERVER['DOCUMENT_ROOT'].$address;

    $cusPass = md5(md5($request->data['password']));

    $data = array(
        'name'=>$request->data['name'],
        'email'=>$request->data['email'],
        'phone_number'=>$request->data['phone_number'],
        'address'=>$request->data['address'],
        'city'=>$request->data['city'],
        'state'=>$request->data['state'],
        'zip'=>$request->data['zip'],
        'comment'=>isset($request->data['comment'])?$request->data['comment']:"",
        'company_name'=>isset($request->data['company_name'])?$request->data['company_name']:"",
        'password'=>$cusPass
    );

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    if(trim($request->files['image_video']['name'])){
        $fileName =$request->files['image_video']['name'];

        $temporaryName = $request->files['image_video']['tmp_name'];
        move_uploaded_file($temporaryName, $path . $fileName);
        $data['avatar'] = $address.$request->files['image_video']['name'];
    }

    $db = Flight::db();

    if( isset($request->data['id']) && trim($request->data['id']) ){
        // UPDATE/EDIT
        // $data['token'] = "3f5e4532253fbb3829fcfff03f18a9e93b4ff3a4f491e84be06077931bfccd2b";
        $data['updated_at'] = date("Y-m-d H:i:s");
        // $data['account'] = "811ABB9D98A1";
        $id = $request->data['id'];
        $flg = $db->update("customers",$data, array('id'=>$id));
    }else{
        //NEW INSERT
        // $data['token'] = "3f5e4532253fbb3829fcfff03f18a9e93b4ff3a4f491e84be06077931bfccd2b";
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        // $data['account'] = "811ABB9D98A1";
        $flg = $db->insert("customers",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Customer Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Customer Save Error Occurred.'
        ));
    }
});

// ............CUSTOMERS END.....................


// ............EMPLOYEE START.....................

Flight::route("POST /employeeList", function(){
    Flight::checkLogin();

    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $db = Flight::db();

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM employees ORDER BY id ASC LIMIT ".$off.",".$lmt);
    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(
        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

Flight::route('GET /employeeEdit/@id/@rand', function($id, $rand){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM employees WHERE id=:id",array('id'=>$id));

    Flight::json($data);
});

Flight::route("POST /employeeEdit", function(){
    //Flight::checkLogin();
    $request = Flight::request();

    $address = "/plexpos/archive/employees/";
    $path = $_SERVER['DOCUMENT_ROOT'].$address;

    // $empPass = md5(md5($request->data['new_password']));
    $provided_pass = $request->data['password'];
    $empPass = password_hash($provided_pass, PASSWORD_DEFAULT);

    $data = array(
        'name'=>$request->data['name'],
        'email'=>$request->data['email'],
        'password'=>$empPass
        // 'phone_number'=>$request->data['phone_number'],
        // 'address'=>$request->data['address'],
        // 'city'=>$request->data['city'],
        // 'state'=>$request->data['state'],
        // 'zip'=>$request->data['zip'],
        // 'comment'=>isset($request->data['comment'])?$request->data['comment']:"",
        // 'company_name'=>isset($request->data['company_name'])?$request->data['company_name']:"",
    );

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    if(trim($request->files['image_video']['name'])){
        $fileName =$request->files['image_video']['name'];

        $temporaryName = $request->files['image_video']['tmp_name'];
        move_uploaded_file($temporaryName, $path . $fileName);
        $data['avatar'] = $address.$request->files['image_video']['name'];
    }

    $db = Flight::db();

    if( isset($request->data['id']) && trim($request->data['id']) ){
        // UPDATE/EDIT
        $data['updated_at'] = date("Y-m-d H:i:s");
        $id = $request->data['id'];
        $flg = $db->update("employees",$data, array('id'=>$id));
    }else{
        //NEW INSERT
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $flg = $db->insert("employees",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Employee Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Employee Save Error Occurred.'
        ));
    }
});

Flight::route('GET /employeeDelete/@id', function($id){
    Flight::checkLogin();

    $db = Flight::db();
    $flg = $db->delete('employees', array('id'=>$id));

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Employee deleted successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Employee could not be deleted.'
        ));
    }
});

// ............EMPLOYEE END.....................

// ..............PAGES START.................

Flight::route('POST /pages', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $db = Flight::db();

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM pages LIMIT ".$off.",".$lmt);

    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(

        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

Flight::route('GET /pages/@id/@rand', function($id,$rand){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM pages WHERE id=:id",array('id'=>$id));

    Flight::json($data);
});

Flight::route('POST /pageChange', function(){
    Flight::checkLogin();
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);

    $db = Flight::db();
    $data = array(
        'title'=>$params->title,
        'content'=>$params->content
    );

    if(isset($params->id)){
        $flg = $db->update("pages",$data, array('id'=>$params->id));
    }else{
        $data['name'] = $params->name;
        $data['title'] = $params->title;
        $data['created_at'] = date("Y-m-d H:i:s");
        $flg = $db->insert("pages",$data);
    }

    if($flg){
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Page Saved Successfully.'
        ));
    }else{
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Page Save Error Occurred.'
        ));
    }
});

//// User start

Flight::route('POST /users', function(){
    Flight::checkLogin();
    $post_data = file_get_contents('php://input');
    parse_str($post_data,$params);

    $db = Flight::db();

    $lmt = isset($params['limit'])?$params['limit']:10;
    $off = isset($params['offset'])?$params['offset']:0;

    $rows = $db->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM users WHERE id != 1 ORDER BY created_at DESC LIMIT ".$off.",".$lmt);

    $total = $db->fetchRow("SELECT FOUND_ROWS() total");
    $data = array(

        'rows'=>$rows?$rows:[],
        'total'=>$total['total']
    );
    Flight::json($data);
});

Flight::route('GET /userEdit/@id/@rand', function($id,$rand){
    Flight::checkLogin();

    $db = Flight::db();

    $data = $db->fetchRow("SELECT * FROM users WHERE id=:id",array('id'=>$id));

    Flight::json($data);
});

Flight::route('POST /userEdit', function(){
    Flight::checkLogin();
    $entityBody = file_get_contents('php://input');
    $params = json_decode($entityBody);

    try{
        $email = filter_var($params->email, FILTER_SANITIZE_EMAIL);
        $fname = strip_tags($params->first_name);
        $lname = strip_tags($params->last_name);
        $data = array(
            'email' => $email,
            'first_name' => $fname,
            'last_name' => $lname,
            'activated' => false
        );

        if(isset($params->id)){
            $user = Cartalyst\Sentry\Facades\Native\Sentry::findUserById($params->id);
            if (isset($params->current_password)) {
                if($user->checkPassword($params->current_password)){
                    $resetCode = $user->getResetPasswordCode();
                    //SEND EMAIL
                    $user->password = strip_tags($params->new_password);
                }
                else{
                    Flight::json(array(
                        'status'  => 'error',
                        'message' => "Your provided password does not match"
                    ));
                    exit;
                }
            }

            $user->email = filter_var($params->email, FILTER_SANITIZE_EMAIL);
            $user->first_name = strip_tags($params->first_name);
            $user->last_name = strip_tags($params->last_name);
            $user->activated = strip_tags($params->status_change);
            $user->activated_at = date("Y-m-d H:i:s");
            $user->updated_at = date("Y-m-d H:i:s");
            // $user->password = strip_tags($_POST['password']);

            if ($user->save()) {
                Flight::json(array(
                    'status'  => 'success',
                    'message' => 'User successfully updated'
                ));
            } else {
                Flight::json(array(
                    'status'  => 'error',
                    'message' => "User could not be updated"
                ));
            }
            // $flg = $db->update("users",$data, array('id'=>$params->id));
        }else{
            $password = strip_tags($params->password);
            $data['password'] = $password;
            $data['created_at'] = date("Y-m-d H:i:s");
            $user = Cartalyst\Sentry\Facades\Native\Sentry::createUser($data);
            $code = $user->getActivationCode();
            $useractivationemail = $email;
            $subject = 'KC activation code';
            $message = "
            <html>
                <head>
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7' crossorigin='anonymous'>
                    <script src='//code.jquery.com/jquery-1.12.0.min.js'></script>
                    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' integrity='sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS' crossorigin='anonymous'></script>
                    <title>Tempo-Responsive Email Template</title>
                        <style type=\"text/css\">
                        /* Client-specific Styles */
                        div, p, a, li, td { -webkit-text-size-adjust:none; }
                        #outlook a {padding:0;} /* Force Outlook to provide a \"view in browser\" menu link. */
                        html{width: 100%; }
                        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
                        /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
                        .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
                        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing. */
                        #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
                        img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
                        a img {border:none;}
                        .image_fix {display:block;}
                        p {margin: 0px 0px !important;}
                        table td {border-collapse: collapse;}
                        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
                        a {color: #33b9ff;text-decoration: none;text-decoration:none!important;}
                        /*STYLES*/
                        table[class=full] { width: 100%; clear: both; }
                        /*IPAD STYLES*/
                        @media only screen and (max-width: 640px) {
                        a[href^=\"tel\"], a[href^=\"sms\"] {
                        text-decoration: none;
                        color: #33b9ff; /* or whatever your want */
                        pointer-events: none;
                        cursor: default;
                        }
                        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {
                        text-decoration: default;
                        color: #33b9ff !important;
                        pointer-events: auto;
                        cursor: default;
                        }
                        table[class=devicewidth] {width: 440px!important;text-align:center!important;}
                        table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
                        img[class=banner] {width: 440px!important;height:220px!important;}
                        img[class=col2img] {width: 440px!important;height:220px!important;}
                        
                        
                        }
                        /*IPHONE STYLES*/
                        @media only screen and (max-width: 480px) {
                        a[href^=\"tel\"], a[href^=\"sms\"] {
                        text-decoration: none;
                        color: #33b9ff; /* or whatever your want */
                        pointer-events: none;
                        cursor: default;
                        }
                        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {
                        text-decoration: default;
                        color: #33b9ff !important; 
                        pointer-events: auto;
                        cursor: default;
                        }
                        table[class=devicewidth] {width: 280px!important;text-align:center!important;}
                        table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
                        img[class=banner] {width: 280px!important;height:140px!important;}
                        img[class=col2img] {width: 280px!important;height:140px!important;}
                        
                        
                        }
                        </style>
                </head>
               <body>    
                    <!-- Start of header -->
                    <table width=\"100%\" bgcolor=\"#fcfcfc\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" st-sortable=\"header\">
                       <tbody>
                          <tr>
                             <td>
                                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"devicewidth\">
                                   <tbody>
                                      <tr>
                                         <td width=\"100%\">
                                            <table width=\"600\" bgcolor=\"#eacb3c\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"devicewidth\">
                                               <tbody>
                                                  <tr>
                                                     <td>
                                                        <!-- logo -->
                                                        <table bgcolor=\"#000000\" width=\"140\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"devicewidth\">
                                                           <tbody>
                                                              <tr>
                                                                 <td width=\"140\" height=\"50\" align=\"center\">
                                                                    <div class=\"imgpop\">
                                                                       <a target=\"_blank\" href=\"http://www.kaporchupor.com/\" style= \"color: white; font-family: cursive; font-size: 20px; font-weight: bold; \" />
                                                                       Kaporchupor
                                                                       </a>
                                                                    </div>
                                                                 </td>
                                                              </tr>
                                                           </tbody>
                                                        </table>
                                                        <!-- end of logo -->
                                                        <!-- start of menu -->
                                                        <table bgcolor=\"#eacb3c\" width=\"250\" height=\"50\" border=\"0\" align=\"right\" valign=\"middle\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"devicewidth\">
                                                           <tbody>
                                                              <tr>
                                                                 
                                                              </tr>
                                                           </tbody>
                                                        </table>
                                                        <!-- end of menu -->
                                                     </td>
                                                  </tr>
                                               </tbody>
                                            </table>
                                         </td>
                                      </tr>
                                   </tbody>
                                </table>
                             </td>
                          </tr>
                       </tbody>
                    </table>
                    <!-- End of Header -->
                    <!-- Start of seperator -->
                    <table width=\"100%\" bgcolor=\"#fcfcfc\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" st-sortable=\"seperator\">
                       <tbody>
                          <tr>
                             <td>
                                <table width=\"600\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"devicewidth\">
                                   <tbody>
                                      <tr>
                                         <td align=\"center\" height=\"30\" style=\"font-size:1px; line-height:1px;\">&nbsp;</td>
                                      </tr>
                                   </tbody>
                                </table>
                             </td>
                          </tr>
                       </tbody>
                    </table>
                    <!-- End of seperator --> 
                    
                    <!-- start of Full text -->
                    <table width=\"100%\" bgcolor=\"#fcfcfc\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" st-sortable=\"full-text\">
                       <tbody>
                          <tr>
                             <td>
                                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"devicewidth\">
                                   <tbody>
                                      <tr>
                                         <td width=\"100%\">
                                            <table bgcolor=\"#ffffff\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"devicewidth\">
                                               <tbody>
                                                  <!-- Spacing -->
                                                  <tr>
                                                     <td height=\"20\" style=\"font-size:1px; line-height:1px; mso-line-height-rule: exactly;\">&nbsp;</td>
                                                  </tr>
                                                  <!-- Spacing -->
                                                  <tr>
                                                     <td>
                                                        <table width=\"560\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"devicewidthinner\">
                                                           <tbody>
                                                              <!-- Title -->
                                                              <tr>
                                                                 <td style=\"font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #282828; text-align:center; line-height: 24px;\">
                                                                    Account Activision Code
                                                                 </td>
                                                              </tr>
                                                              <!-- End of Title -->
                                                              <!-- spacing -->
                                                              <tr>
                                                                 <td width=\"100%\" height=\"15\" style=\"font-size:1px; line-height:1px; mso-line-height-rule: exactly;\">&nbsp;</td>
                                                              </tr>
                                                              <!-- End of spacing -->
                                                              <!-- content -->
                                                              <tr>
                                                                 <td style=\"font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #889098; text-align:center; line-height: 24px;\">
                                                                    Click on this Link to activate your account. 
                                                                    <p><strong> <a href= http://kaporchupor.com/plexpos/api/activatebycode/".$code."><i>Click on this link to activate your account. This link will be invalid after 4 hours</i></a> </strong></p>
                                                                 </td>
                                                              </tr>
                                                              <!-- End of content -->
                                                              <!-- Spacing -->
                                                              <tr>
                                                                 <td width=\"100%\" height=\"15\" style=\"font-size:1px; line-height:1px; mso-line-height-rule: exactly;\">&nbsp;</td>
                                                              </tr>
                                                              <!-- Spacing -->
                                                           </tbody>
                                                        </table>
                                                     </td>
                                                  </tr>
                                                  <!-- Spacing -->
                                                  <tr>
                                                     <td height=\"20\" style=\"font-size:1px; line-height:1px; mso-line-height-rule: exactly;\">&nbsp;</td>
                                                  </tr>
                                                  <!-- Spacing -->
                                               </tbody>
                                            </table>
                                         </td>
                                      </tr>
                                   </tbody>
                                </table>
                             </td>
                          </tr>
                       </tbody>
                    </table>
                    <!-- End of Full Text -->
                    <!-- Start of seperator -->
                    <table width=\"100%\" bgcolor=\"#fcfcfc\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" st-sortable=\"seperator\">
                       <tbody>
                          <tr>
                             <td>
                                <table width=\"600\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"devicewidth\">
                                   <tbody>
                                      <tr>
                                         <td align=\"center\" height=\"30\" style=\"font-size:1px; line-height:1px;\">&nbsp;</td>
                                      </tr>
                                   </tbody>
                                </table>
                             </td>
                          </tr>
                       </tbody>
                    </table>
                    <!-- End of seperator --> 
                    <!-- Start of footer -->
                    <table width=\"100%\" bgcolor=\"#fcfcfc\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" st-sortable=\"footer\">
                       <tbody>
                          <tr>
                             <td>
                                <table width=\"600\" bgcolor=\"#eacb3c\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"devicewidth\">
                                   <tbody>
                                      <tr>
                                         <td width=\"100%\">
                                            <table bgcolor=\"#eacb3c\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"devicewidth\">
                                               <tbody>
                                                  <!-- Spacing -->
                                                  <tr>
                                                     <td height=\"10\" style=\"font-size:1px; line-height:1px; mso-line-height-rule: exactly;\">&nbsp;</td>
                                                  </tr>
                                                  <!-- Spacing -->
                                                  <tr>
                                                     <td>
                                                        <!-- Social icons -->
                                                        <table  width=\"150\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"devicewidth\">
                                                           <tbody>
                                                              <tr>
                                                                 <td width=\"43\" height=\"43\" align=\"center\">
                                                                    <div class=\"imgpop\">
                                                                       <a target=\"_blank\" href=\"https://www.facebook.com/kaporC\">
                                                                       <img src=\"img/facebook.png\" alt=\"\" border=\"0\" width=\"43\" height=\"43\" style=\"display:block; border:none; outline:none; text-decoration:none;\">
                                                                       </a>
                                                                    </div>
                                                                 </td>
                                                                 <td align=\"left\" width=\"20\" style=\"font-size:1px; line-height:1px;\">&nbsp;</td>
                                                                 <td width=\"43\" height=\"43\" align=\"center\">
                                                                    <div class=\"imgpop\">
                                                                       <a target=\"_blank\" href=\"https://twitter.com/kaporchupor\">
                                                                       <img src=\"img/twitter.png\" alt=\"\" border=\"0\" width=\"43\" height=\"43\" style=\"display:block; border:none; outline:none; text-decoration:none;\">
                                                                       </a>
                                                                    </div>
                                                                 </td>
                                                                 <td align=\"left\" width=\"20\" style=\"font-size:1px; line-height:1px;\">&nbsp;</td>
                                                                 <td width=\"43\" height=\"43\" align=\"center\">
                                                                    <div class=\"imgpop\">
                                                                       <a target=\"_blank\" href=\"https://www.linkedin.com/groups/7040059/profile\">
                                                                       <img src=\"img/linkedin.png\" alt=\"\" border=\"0\" width=\"43\" height=\"43\" style=\"display:block; border:none; outline:none; text-decoration:none;\">
                                                                       </a>
                                                                    </div>
                                                                 </td>
                                                              </tr>
                                                           </tbody>
                                                        </table>
                                                        <!-- end of Social icons -->
                                                     </td>
                                                  </tr>
                                                  <!-- Spacing -->
                                                  <tr>
                                                     <td height=\"10\" style=\"font-size:1px; line-height:1px; mso-line-height-rule: exactly;\">&nbsp;</td>
                                                  </tr>
                                                  <!-- Spacing -->
                                               </tbody>
                                            </table>
                                         </td>
                                      </tr>
                                   </tbody>
                                </table>
                             </td>
                          </tr>
                       </tbody>
                    </table>
                    <!-- End of footer -->
                    <!-- Start of Postfooter -->
                    <table width=\"100%\" bgcolor=\"#fcfcfc\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" st-sortable=\"postfooter\" >
                       <tbody>
                          <tr>
                             <td>
                                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"devicewidth\">
                                   <tbody>
                                      <!-- Spacing -->
                                      <tr>
                                         <td width=\"100%\" height=\"20\"></td>
                                      </tr>
                                      <!-- Spacing -->
                                      <tr>
                                         <td align=\"center\" valign=\"middle\" style=\"font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #282828\" st-content=\"preheader\">
                                            Don't want to receive email Updates? <a href=\"#\" style=\"text-decoration: none; color: #eacb3c\">Unsubscribe here </a> 
                                         </td>
                                      </tr>
                                      <!-- Spacing -->
                                      <tr>
                                         <td width=\"100%\" height=\"20\"></td>
                                      </tr>
                                      <!-- Spacing -->
                                   </tbody>
                                </table>
                             </td>
                          </tr>
                       </tbody>
                    </table>
                    <!-- End of postfooter -->      
               </body>
            </html>
            ";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: plexpos-admin@kaporchupor.com';
            $headers .= 'Cc: admin@kc.com' . "\r\n";
            if (!mail($email, $subject, $message, $headers)) {
              throw new Exception('Email could not be sent.');
            }

            Flight::json(array(
                'status'  => 'success',
                'message' => 'your account is created but will not be activated until you click the activation code sent to the given address: '.$email
            ));
        }
    } catch (Exception $e) {
        Flight::json(array(
            'status'  => 'error',
            'message' => $e->getMessage()
        ));
    }
});

Flight::route('GET /activatebycode/@code', function($code){
    global $header_location;
    global $useractivationemail;
    try{
        $user = Cartalyst\Sentry\Facades\Native\Sentry::findUserByActivationCode($code);
        if ($user->attemptActivation($code)) {
            header('Location: '.$header_location.'/useractive.php?userEmail='.$useractivationemail.'&base='.$header_location);
        } else {
            header('Location: '.$header_location.'/usernotfound.php');
        }
    }
    catch (Cartalyst\Sentry\Users\UserNotFoundException $e){
        Flight::json(array(
            'status'  => 'error',
            'message' => $e
        ));
        //header('Location: '.$header_location.'/usernotfound.php');
    }
});

Flight::route('GET, /activateUser', function($code){
    try{
        $user = Cartalyst\Sentry\Facades\Native\Sentry::findUserByActivationCode($code);
        if ($user->attemptActivation($code)) {
            Flight::json(array(
                'status'  => 'success',
                'message' => 'Your account has activated successfully'
            ));
        } else {
            Flight::json(array(
                'status'  => 'error',
                'message' => 'Your account activation failed'
            ));
        }
    }
    catch (Cartalyst\Sentry\Users\UserNotFoundException $e){
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User can not be found'
        ));
    }
});

Flight::route('GET /userDelete/@id', function($id){
    try {
        $user = Cartalyst\Sentry\Facades\Native\Sentry::findUserById($id);
        $user->delete();
        Flight::json(array(
            'status'  => 'success',
            'message' => 'User successfully deleted.'
        ));
    } catch (Exception $e) {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User can not be deleted..'
        ));
    }
});

//// User end

// ..............PAGES END.................

Flight::start();
?>
