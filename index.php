<?php
require 'cors_header.php';

?><!DOCTYPE html>
<html>
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>PlexPos</title>

    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="/plexpos/assets/images/favicon.png">

    <!-- Loader CSS -->
    <link rel="stylesheet" href="/plexpos/assets/css/loader.css" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/plexpos/assets/css/bootstrap.css" />
    <link rel="stylesheet" href="/plexpos/assets/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="/plexpos/assets/css/font-awesome.css" />
    <link rel="stylesheet" href="/plexpos/assets/fonts/flaticon.css" />
    <link rel="stylesheet" href="/plexpos/assets/css/select2.css" />
    <link rel="stylesheet" href="/plexpos/assets/css/jquery.gritter.css" />
    <!-- <link rel="stylesheet" href="/plexpos/assets/lib/bootgrid/jquery.bootgrid.min.css" /> -->
    <link rel="stylesheet" href="/plexpos/assets/lib/bootstrap-table/bootstrap-table.min.css" />

    <!-- <link rel="stylesheet" href="/plexpos/assets/css/bootstrap-editable.css" /> -->
    <link rel="stylesheet" href="/plexpos/assets/css/stockControl.css" />

    <link rel="stylesheet" href="/plexpos/assets/css/chosen.css" />

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="/plexpos/assets/css/bootstrap-duallistbox.css" />

    <!-- text fonts -->
    <link rel="stylesheet" href="/plexpos/assets/css/ace-fonts.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="/plexpos/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

    <link rel="stylesheet" type="text/css" href="/plexpos/assets/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.css">

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/plexpos/assets/css/ace-part2.css" class="ace-main-stylesheet" />
    <![endif]-->

    <!--[if lte IE 9]>
            <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
    <![endif]-->
    <link rel="stylesheet" href="assets/css/ace-skins.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.css" />


    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/plexpos/assets/css/ace-ie.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="/plexpos/assets/js/ace-extra.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="/plexpos/assets/js/html5shiv.js"></script>
    <script src="/plexpos/assets/js/respond.js"></script>
    <![endif]-->

    <!--[if !IE]> -->
    <script src="/plexpos/assets/js/jquery.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="/plexpos/assets/js/jquery1x.js"></script>
    <![endif]-->
    <script type="text/javascript">
        if('ontouchstart' in document.documentElement) document.write("<script src='/plexpos/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
    </script>
    <script type="text/javascript">
        var url = 'http://192.168.1.128';
        // var url = 'http://localhost';
//         var url = 'http://kaporchupor.com';
        var curr_count_id;
        var curr_count_name;
        var curr_count_type;

        var current_user;
        $.ajax({
            url : url+"/plexpos/api/checkLogin",
            type: "GET",
            success:function(data, textStatus, jqXHR){
                if (data.status == true) {
                    current_user = data;
                } else {
                    window.location.assign("login.html");
                }
            },
            error: function(jqXHR, textStatus, errorThrown){

            }
        });
    </script>

</head>
<body class="no-skin loading">

<!-- #section:basics/navbar.layout -->
<div id="navbar" class="navbar navbar-default ace-save-state navbar-fixed-top navbar-collapse">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <!-- #section:basics/sidebar.mobile.toggle -->
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <!-- /section:basics/sidebar.mobile.toggle -->
        <div class="navbar-header pull-left">
            <!-- #section:basics/navbar.layout.brand -->
            <img class="pull-left" src="assets/img/POS%20Terminal-48.png"/>
            <a href="#" class="navbar-brand">

                <small>
                    PlexPos Admin
                </small>
            </a>

            <!-- /section:basics/navbar.layout.brand -->

            <!-- #section:basics/navbar.toggle -->

            <!-- /section:basics/navbar.toggle -->
        </div>

        <!-- #section:basics/navbar.dropdown -->
        <div class="navbar-buttons navbar-header pull-right navbar-collapse collapse" role="navigation">
            <ul class="nav ace-nav">
                <!-- #section:basics/navbar.user_menu -->
                <li class="grey dropdown-modal">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="ace-icon fa fa-bell icon-animated-bell"></i>
                        <span id="online_del_del_count" class="badge badge-grey"></span>
                    </a>

                    <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                        <li id="online_del_del_head" class="dropdown-header">
                            <i class="ace-icon fa fa-check"></i>
                        </li>

                        <li class="dropdown-content">
                            <ul id="onlinenoti_panel" class="dropdown-menu dropdown-navbar">
<!--                                Dynamic Content Loading-->
                            </ul>
                        </li>

                        <li class="dropdown-footer">
                            <a href="#" onclick="load_online_order_view();">
                                See all Online orders
                                <i class="ace-icon fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                    <img class="nav-user-photo" src="assets/images/avatars/user.png" alt="admin Photo" />
                        <span class="user-info"></span>
                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                        <li>
                            <a href="/plexpos/logout.php">
                                <i class="ace-icon fa fa-power-off"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- /section:basics/navbar.user_menu -->
            </ul>
        </div>

        <!-- /section:basics/navbar.dropdown -->
    </div><!-- /.navbar-container -->
</div>

<!-- /section:basics/navbar.layout -->
<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>

    <!-- #section:basics/sidebar -->
    <div id="sidebar" class="sidebar responsive sidebar-fixed ace-save-state menu-min">  <!--"menu-min"..for min menu, "compact" class for compact sidebar -->
        <script type="text/javascript">
            try{ace.settings.loadState('sidebar')}catch(e){}
        </script>


        <ul id="sidebar" class="nav nav-list">
            <li class="single">        <!-- Dashboard -->
                <a class="ajax-nav-link" href="dashboard.html">
                    <i class="menu-icon fa fa-dashboard" aria-hidden="true"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>
            </li>

            <li class="single">        <!-- Cash Register -->
                <a class="ajax-nav-link" href="cash_register_list.html">
                    <i class="menu-icon flaticon-cash-register"></i>
                    <span class="menu-text"> Cash Register </span>
                </a>
            </li>

            <li>        <!-- Sell -->
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon flaticon-full-items-inside-a-shopping-bag"></i>
                    <span class="menu-text"> Sell &nbsp </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li>
                        <a href="sell_order_list.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Sales History
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li>
                        <a href="order_return.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Sales Return
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <!-- <li class="">
                        <a href="sales_ledger.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Sales Ledger
                        </a>
                        <b class="arrow"></b>
                    </li> -->
                    <!-- <li>
                        <a class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Settings
                        </a>
                        <b class="arrow"></b>
                    </li> -->
                </ul>
            </li>

            <li>        <!-- Site Management -->
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-cogs"></i>
                    <span class="menu-text"> Site Management &nbsp </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li>        <!-- Menu -->
                        <a href="manage_menus.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-bars"></i>
                            Menus
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li>        <!-- categories -->
                        <a class="ajax-nav-link" href="manage_categories.html">
                            <i class="menu-icon fa fa-wrench"></i>
                            Categories
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li>        <!-- Page -->
                        <a class="ajax-nav-link" href="page_management.html">
                            <i class="menu-icon fa fa-paper-plane-o"></i>
                            Page Management
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li>        <!-- widget Management -->
                        <a class="ajax-nav-link" href="widget_management.html">
                            <i class="menu-icon fa fa-cog"></i>
                            Widget Management
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li>        <!-- Logo && Modgurd -->
                        <a class="ajax-nav-link" href="logo_manage.html">
                            <i class="menu-icon fa fa-cog"></i>
                            Logo Management
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>

            <li>        <!-- Reporting -->
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-area-chart"></i>
                    <span class="menu-text"> Reporting &nbsp </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a href="sales_report.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Sales Report
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="inventory_report.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Inventory Report
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="payment_report.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Payment Report
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="online_deliveries.html" class="ajax-nav-link">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Online Deliveries
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>

            <li>        <!-- PRODUCTS -->
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon flaticon-barcode"></i>
                    <span class="menu-text"> Products &nbsp </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a class="ajax-nav-link" href="product_list.html">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Products
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a class="ajax-nav-link" href="product_inventory_list.html">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Stock Control
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a class="ajax-nav-link" href="product_settings.html">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Product Settings
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>

            <li>        <!-- Management -->
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-cogs"></i>
                    <span class="menu-text"> Management &nbsp </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a class="ajax-nav-link" href="customer_management.html">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Customer
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a class="ajax-nav-link" href="employee_management.html">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Employee
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>

            <li class="single">        <!-- Discount -->
                <a class="ajax-nav-link" href="discount_category_list.html">
                    <i class="menu-icon flaticon-scissors"></i>
                    <span class="menu-text"> Discount Category </span>
                </a>
            </li>

            <li class="single">        <!-- User -->
                <a class="ajax-nav-link" href="user_management.html">
                    <i class="menu-icon fa fa-user-md"></i>
                    <span class="menu-text"> User Management </span>
                </a>
            </li>

        </ul><!-- /.nav-list -->

        <ul class="nav nav-list" id="nav-bar">

        </ul><!-- /.nav-list -->

        <!-- #section:basics/sidebar.layout.minimize -->
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

        <!-- /section:basics/sidebar.layout.minimize -->
    </div>

    <!-- /section:basics/sidebar -->
    <div class="main-content">
        <div class="main-content-inner">

            <!-- /section:basics/content.breadcrumbs -->
            <div class="page-content" id="ajax-content">
                <h1>Welcome to the PlexPOS Admin dashboard.</h1>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    <div class="footer">    <!-- footer -->
        <div class="footer-inner">
            <div class="footer-content">
                <!-- footer content here -->
                <span class="bigger-120"><span class="blue bolder">Plexpos Admin</span> <a target="blank" href="http://kiteplexit.com/">KiteplexIT</a> © 2016-2017</span>

                <span class="action-buttons">
                    <a href="#">
                        <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                    </a>

                    <a href="https://www.facebook.com/pages/Kiteplex-It-Ltd/1675194546031011">
                        <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                    </a>

                    <a href="#">
                        <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>  <!-- footer -->


    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<div class="modal"><!-- Place at bottom of page --></div>


<!-- basic scripts -->


<script src="/plexpos/assets/js/bootstrap.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="/plexpos/assets/js/excanvas.js"></script>
<![endif]-->
<script src="/plexpos/assets/js/jquery-ui.custom.js"></script>
<script src="/plexpos/assets/js/jquery.ui.touch-punch.js"></script>

<script src="/plexpos/assets/js/history/history.js"></script>
<script src="/plexpos/assets/js/history/history.adapter.native.js"></script>

<!-- Handlebars -->
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>-->
<script type="text/javascript" src="/plexpos/assets/lib/handlebars/handlebars.runtime-v4.0.5.js"></script>

<!-- Alpaca -->
<!--<script type="text/javascript" src="http://code.cloudcms.com/alpaca/1.5.15/bootstrap/alpaca.min.js"></script>-->
<script type="text/javascript" src="/plexpos/assets/lib/alpaca/bootstrap/alpaca.min.js"></script>
<!-- <script type="text/javascript" src="/plexpos/assets/lib/alpaca/bootstrap/alpaca_file_upload.js"></script> -->
<!--<link type="text/css" href="http://code.cloudcms.com/alpaca/1.5.15/bootstrap/alpaca.min.css" rel="stylesheet" />-->
<link type="text/css" href="/plexpos/assets/lib/alpaca/bootstrap/alpaca.min.css" rel="stylesheet" />


<script src="/plexpos/assets/lib/ckeditor/ckeditor.js"></script>
<script src="/plexpos/assets/lib/ckeditor/adapters/jquery.js"></script>

<!-- bootstrap-multiselect for time field -->
<script src="/plexpos/assets/lib/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" media="screen" href="/plexpos/assets/lib/bootstrap-multiselect/css/bootstrap-multiselect.css"/>

<!-- Moment js Loading -->
<script type="text/javascript" src="/plexpos/assets/lib/moment/min/moment-with-locales.min.js"></script>

<!-- bootstrap date time picker -->
<script type="text/javascript" src="/plexpos/assets/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>

<!-- Required for the Editor field (sample code viewing) -->
<script src="/plexpos/assets/lib/ace-builds/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>

<!-- Beautify (used by EditorField) -->
<script src="/plexpos/assets/lib/js-beautify/js/lib/beautify.js" type="text/javascript" charset="utf-8"></script>
<script src="/plexpos/assets/lib/js-beautify/js/lib/beautify-css.js" type="text/javascript" charset="utf-8"></script>
<script src="/plexpos/assets/lib/js-beautify/js/lib/beautify-html.js" type="text/javascript" charset="utf-8"></script>

<!-- typeahead.js -->
<script src="/plexpos/assets/lib/typeahead.js/dist/bloodhound.min.js" type="text/javascript"></script>
<script src="/plexpos/assets/lib/typeahead.js/dist/typeahead.bundle.min.js" type="text/javascript"></script>

<!-- datatables (for TableField) -->
<script src="/plexpos/assets/lib/datatables/media/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="/plexpos/assets/lib/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>

<!-- <script src="/plexpos/assets/lib/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js"></script> -->

<!-- BOOTSTRAP TABLE EXPORT PLUGIN -->
<script src="/plexpos/assets/lib/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<script src="/plexpos/assets/lib/bootstrap-table/extensions/export/libs/FileSaver/FileSaver.min.js"></script>
<script src="/plexpos/assets/lib/bootstrap-table/extensions/export/jspdf.min.js"></script>
<script src="/plexpos/assets/lib/bootstrap-table/extensions/export/jspdf.plugin.autotable.js"></script>
<script src="/plexpos/assets/lib/bootstrap-table/extensions/export/libs/html2canvas/html2canvas.min.js"></script>
<script src="/plexpos/assets/lib/bootstrap-table/extensions/export/tableExport.min.js"></script>
<!-- BOOTSTRAP TABLE EXPORT PLUGIN -->

<!--<link type="text/css" href="/plexpos/assets/lib/datatables/media/css/jquery.dataTables.css" rel="stylesheet"/>-->
<script src="/plexpos/assets/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<link type="text/plexpos/assets/css" href="/plexpos/assets/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet"/>

<link rel="stylesheet" href="/plexpos/assets/lib/jstree-bootstrap/dist/themes/proton/style.css" />
<script src="/plexpos/assets/lib/jstree-bootstrap/dist/jstree.min.js"></script>

<!-- <script src="/plexpos/assets/lib/bootgrid/jquery.bootgrid.min.js"></script> -->

<!-- ui.jqgrid -->
<link rel="stylesheet" type="text/css" href="/plexpos/assets/css/ui.jqgrid.min.css">
<script type="text/javascript" src="/plexpos/assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<!-- ui.jqgrid -->

<!-- jquery-ui.js -->
<script type="text/javascript" src="/plexpos/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<!-- jquery-ui.js -->

<!--colorpicker-->
<link rel="stylesheet" href="/plexpos/assets/lib/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" />
<script type="text/javascript" src="/plexpos/assets/lib/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- select2 -->
<script src="/plexpos/assets/js/select2.js"></script>
<!-- <script src="/plexpos/assets/js/bootstrap-table-select2-filter.js"></script> -->
<!-- select2 -->

<!--TIme Ago script-->
<script src="/plexpos/assets/js/timeago.js"></script>

<script src="/plexpos/assets/js/jquery.bootstrap-duallistbox.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.js"></script>
<script src="/plexpos/assets/js/ace/elements.scroller.js"></script>
<script src="/plexpos/assets/js/ace/elements.colorpicker.js"></script>
<script src="/plexpos/assets/js/ace/elements.fileinput.js"></script>
<script src="/plexpos/assets/js/ace/elements.typeahead.js"></script>
<script src="/plexpos/assets/js/ace/elements.wysiwyg.js"></script>
<script src="/plexpos/assets/js/ace/elements.spinner.js"></script>
<script src="/plexpos/assets/js/ace/elements.treeview.js"></script>
<script src="/plexpos/assets/js/ace/elements.wizard.js"></script>
<script src="/plexpos/assets/js/ace/elements.aside.js"></script>
<script src="/plexpos/assets/js/ace/ace.js"></script>
<script src="/plexpos/assets/js/ace/ace.ajax-content.js"></script>
<script src="/plexpos/assets/js/ace/ace.touch-drag.js"></script>
<script src="/plexpos/assets/js/ace/ace.sidebar.js"></script>
<script src="/plexpos/assets/js/ace/ace.sidebar-scroll-1.js"></script>
<script src="/plexpos/assets/js/ace/ace.submenu-hover.js"></script>
<script src="/plexpos/assets/js/ace/ace.widget-box.js"></script>
<script src="/plexpos/assets/js/ace/ace.settings.js"></script>
<script src="/plexpos/assets/js/ace/ace.settings-rtl.js"></script>
<script src="/plexpos/assets/js/ace/ace.settings-skin.js"></script>
<script src="/plexpos/assets/js/ace/ace.widget-on-reload.js"></script>
<script src="/plexpos/assets/js/ace/ace.searchbox-autocomplete.js"></script>
<script src="/plexpos/assets/js/jquery.nestable.js?v2"></script>
<script src="/plexpos/assets/js/chosen.jquery.js"></script>

<script type="text/javascript"> APP_SERVER_BASE = url; </script>
<!-- <script type="text/javascript"> APP_SERVER_BASE = 'http://localhost'; </script> -->
<script type="text/javascript"> ADMIN_BASE = APP_SERVER_BASE + '/plexpos'; </script>
<script type="text/javascript"> APP_BASE_API = ADMIN_BASE + '/api'; </script>
<script type="text/javascript"> APP_BASE_API_PRODUCT = APP_BASE_API + '/products'; </script>
<script type="text/javascript"> APP_BASE_VIEW = ADMIN_BASE + '/views'; </script>

<!-- Gritter Notification -->
<script type="text/javascript" src="/plexpos/assets/js/jquery.gritter.js"></script>
<!-- Gritter Notification -->

<!-- Bootbox Notification -->
<script type="text/javascript" src="/plexpos/assets/js/bootbox.js"></script>
<!-- Bootbox Notification -->

<!-- DROPZONE -->
<!-- <link rel="stylesheet" href="/plexpos/assets/css/dropzone.css" />
<script src="/plexpos/assets/js/dropzone.js"></script> -->
<!-- DROPZONE -->

<!-- fileupload plugin -->
<link rel="stylesheet" type="text/css" href="/plexpos/assets/lib/jquery-file-upload/css/jquery.fileupload.css">
<script type="text/javascript" src="/plexpos/assets/lib/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.fileupload.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<script src="/plexpos/assets/lib/jquery-file-upload/js/jquery.fileupload-jquery-ui.js"></script>
<!-- fileupload plugin -->

<!--   BOOTSTRAP TABLE -->
<link rel="stylesheet" type="text/css" href="/plexpos/assets/lib/bootstrap-table/extensions/editable/css/bootstrap-editable.css">
<script type="text/javascript" src="/plexpos/assets/lib/bootstrap-table/extensions/editable/js/bootstrap-editable.min.js"></script>

<!--   HIGHCHART.JS -->
<script type="text/javascript" src="/plexpos/assets/lib/highchart/js/highcharts.js"></script>
<script type="text/javascript" src="/plexpos/assets/lib/highchart/js/modules/exporting.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    $('.user-info').html('<small>Welcome, </small>'+current_user.first_name)
    $body = $("body");

    $(document).ready(function(){
        gritter('Logged-In', 'Login Successfull', false, '1500', 'gritter-info, gritter-center');
        $body.removeClass("loading");

        $('ul#sidebar li ul li a').click(function(){
            $(this).parent().addClass('active').siblings().removeClass('active');
            $(this).parent().parent().parent().addClass('active').siblings().removeClass('active');
        });

        $('.single a').click(function(){
            $(this).parent().addClass('active').siblings().removeClass('active');
        });
        fetchPendingdeliveries(0);
    });

    $(document).on({
        ajaxStart: function() { $body.addClass("loading");    },
        ajaxStop: function() { $body.removeClass("loading"); }
    });

    String.prototype.parseBoolean = function ()
    {
        return parseInt(this) ? true : false
    }
        var nestableTree = {
            id: "",
            uri: "",
            dt : [],
            menus : [],
            menu_li : "",
            callback_get_funtion_name: "",
            callback_delete_function_name: "",
            build_tree: function(data,flg){
                //console.log(data);
                var tree = nestableTree.parse_to_tree(data);

                tree.sort(function(a, b) {
                    return parseInt(a.position) - parseInt(b.position);
                });
                // console.debug(tree);
                nestableTree.build_dd_menu(tree,flg);
            },
            parse_to_tree : function(data)
            {
                var map = {};

                for(var i = 0; i < data.length; i++){
                    var obj = data[i];

                    if((typeof map[obj.id]) === "undefined"){
                        map[obj.id] = obj;
                        obj.children = [];
                    }else{

                        obj.children = map[obj.id].children;

                        map[obj.id] = obj;
                    }

                    var parent = obj.parent_id;
                    if((typeof map[parent]) === "undefined"){
                        map[parent] = {
                            children: []
                        };
                    }
                    map[parent].children.push(obj);
                }

                return map['-1'].children;
            },

            build_dd_menu : function(obj,flg)
            {
                nestableTree.menu_li = "";
                $.each(obj, function (index, item) {
                    nestableTree.menu_li += nestableTree.build_dd_menu_item(item);
                });
                $('#' + nestableTree.id + ' ol').html(nestableTree.menu_li);

                if(flg){
                    $('#' + nestableTree.id).nestable().nestable('collapseAll');
                }else{
                    var list = $('#' + nestableTree.id).data('nestable');
                    $('.dd-item').each(function() {
                        var item = $(this),
                            parent = item.parent();
                        if (list == undefined) {
                            bootbox.confirm("Sorry!! Server is currently offline. Want to try again", function(result) {
                                if(result) {
                                    load_view('/manage_menus.html');
                                    $body.removeClass("loading");
                                } else {
                                    load_view('/manage_menus.html');
                                    $body.removeClass("loading");
                                }
                            });
                        }
                        list.setParent(item);
                        if (parent.hasClass(list.options.collapsedClass)) {
                            list.collapseItem(parent.parent());
                        }
                    });
                    $('#' + nestableTree.id).nestable('collapseAll');
                }

                nestableTree.refresh_list();

                $('.dd').off("change");
                $('.dd').on('change', function() {

                    var dd = $('.dd').nestable('serialize');
                    var i = 1;
                    nestableTree.menus = [];
                    $.each(dd, function (index, item) {
                        if(item.children !== undefined){
                            nestableTree.convert_flat_menu_item(item.id, item.children);
                        }
                        nestableTree.menus.push({"id": item.id,"position": i,"parent_id":-1});
                        i = i + 1;
                    });
                    // onChange
                    $.ajax({
                        url : APP_BASE_API + nestableTree.uri,
                        type: "POST",
                        data : JSON.stringify(nestableTree.menus),
                        dataType: "json",
                        processData: false,
                        contentType: 'application/json',
                        success:function(data, textStatus, jqXHR)
                        {
                            nestableTree.refresh_list();
                            $('.preloader').hide();
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            $('.preloader').hide();
                        }
                    });
                });
                $('.dd-handle a').off("mousedown");
                $('.dd-handle a').on('mousedown', function(e){
                    e.stopPropagation();
                });
            },
            build_dd_menu_item : function(item)
            {

                var html = "<li class='dd-item dd2-item' data-id='" + item.id + "' id='" + item.id + "'>";
                html += "<div class='dd-handle dd2-handle'>";
                html += '<i class="normal-icon ace-icon fa fa-bars blue bigger-130"></i>';
                html += '<i class="drag-icon ace-icon fa fa-arrows bigger-125"></i>';
                html += '</div>';
                html += '<div class="dd2-content">';
                html += item.name;
                html += '<div class="pull-right action-buttons">';
                //console.log(item.parent_id);
                html += '<a href="#" class="blue" onclick="'+nestableTree.callback_get_funtion_name+'(\''+item.id+'\');"><i class="ace-icon fa fa-pencil bigger-130"></i></a>';
                html += '<a href="#" id="del-btn-' + item.id + '" class="red" onclick="'+nestableTree.callback_delete_function_name+'(\''+item.id+'\');"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>';
                html += '</div>';
                html += '</div>';

                if (item.children && (item.children.length > 0)) {
                    html += "<ol class='dd-list'>";
                    $.each(item.children, function (index, sub) {
                        html += nestableTree.build_dd_menu_item(sub);
                    });
                    html += "</ol>";
                }
                html += "</li>";

                return html;
            },
            refresh_list : function()
            {
                var dd = $('.dd').nestable('serialize');

                nestableTree.hide_del_btn(dd);
            },
            hide_del_btn: function(dd)
            {
                $.each(dd, function (index, item) {
                    //console.debug(item.children);
                    if(item.children === undefined){
                        $('#del-btn-' + item.id).show();
                    }else{
                        $('#del-btn-' + item.id).hide();
                        nestableTree.hide_del_btn(item.children);
                    }
                });
            },

            convert_flat_menu_item : function(parent, dd)
            {
                var i = 1;
                $.each(dd, function (index, item) {
                    //                console.debug(item);
                    if(item.children !== undefined){
                        nestableTree.convert_flat_menu_item(item.id, item.children);
                    }
                    nestableTree.menus.push({"id": item.id,"position": i,"parent_id":parent});
                    i = i + 1;
                });
            },

            destroy: function(){
                $('#' + nestableTree.id + ' ol').html('');
            },

            init: function(){

            }
        };
        var currentDataUrl = "";
        var currentCategoryId = -1;
        var currentCategoryName = "";

        function requestApi(url, type, data, callback){
            geturl =$.ajax({
                type: type,
                url: APP_BASE_API + url,
                contentType: "application/json",
                dataType: "json",
                data: data,
                beforeSend: function( xhr ) {

                },
                success: function(data, textStatus, jqXHR){
                    //console.log(geturl.getAllResponseHeaders());
                    callback(data, textStatus, jqXHR);
                },
                complete: function() {

                }
            });
        }


        function load_view(viewurl)
        {
            $.ajax({
                url : APP_BASE_VIEW + viewurl,
                type: "GET",
                success:function(data, textStatus, jqXHR)
                {
                    $('#ajax-content').html(data);
                    $('.preloader').hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $('.preloader').hide();
                }
            });
        }

        function load_view_n_data(viewurl,dataurl)
        {
            currentDataUrl = APP_BASE_API + dataurl;
            $.ajax({
                url : APP_BASE_VIEW + viewurl,
                type: "GET",
                success:function(data, textStatus, jqXHR)
                {
                    $('#ajax-content').html(data);
                    $('.preloader').hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $('.preloader').hide();
                }
            });
        }

        function load_archive_data(nd, category_id, type_id)
        {
            currentCategoryId = category_id;
            var category_name = $(nd).children("span").html();
            currentCategoryName = category_name;

            var viewUrl = "";
            var viewTitle = "";
            if(type_id==0){
                // document
                viewTitle = "Upload Document: "+currentCategoryName;
                currentDataUrl = APP_BASE_API + "/archive/category_1/" + category_id;
                viewUrl = APP_BASE_VIEW + "/archive_new_s.html";
                // check if data exist than load in edit form
            }else if(type_id==1){
                // video
                viewTitle = "Upload Video: "+currentCategoryName;
                currentDataUrl = APP_BASE_API + "/archive/category_1/" + category_id;
                viewUrl = APP_BASE_VIEW + "/archive_new_v.html";
                // check if data exist than load in edit form
            }else if(type_id==2){
                // image
                viewTitle = "Image Gallery: "+currentCategoryName;
                currentDataUrl = APP_BASE_API + "/archive/category/" + category_id;
                viewUrl = APP_BASE_VIEW + "/archive_list.html";
            }


            $.ajax({
                url : viewUrl,
                type: "GET",
                success:function(data, textStatus, jqXHR)
                {
                    $('#ajax-content').html(data);
                    $('#category_name').html(viewTitle);
                    $('.preloader').hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $('.preloader').hide();
                }
            });
        }
        function load_archive_data_2()
        {
    //        currentCategoryId = category_id;
    //        var category_name = $(nd).children("span").html();
    //        alert(category_name);
            currentDataUrl = APP_BASE_API + "/archive/category/" + currentCategoryId;
            $.ajax({
                url : APP_BASE_VIEW + "/archive_list.html",
                type: "GET",
                success:function(data, textStatus, jqXHR)
                {
                    $('#ajax-content').html(data);
                    $('#category_name').html("Manage Archive for: "+currentCategoryName);
                    $('.preloader').hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $('.preloader').hide();
                }
            });
        }
        function edit_archive(archive_id)
        {
            currentDataUrl = APP_BASE_API + "/archive/" + archive_id;
            $.ajax({
                url : APP_BASE_VIEW + "/archive_edit.html",
                type: "GET",
                success:function(data, textStatus, jqXHR)
                {
                    $('#ajax-content').html(data);
                    $('.preloader').hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $('.preloader').hide();
                }
            });
        }
        function new_archive()
        {
    //        currentDataUrl = "";
            $.ajax({
                url : APP_BASE_VIEW + "/archive_new.html",
                type: "GET",
                success:function(data, textStatus, jqXHR)
                {
                    $('#ajax-content').html(data);
                    $('#category_id').val(currentCategoryId);
                    $('.preloader').hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $('.preloader').hide();
                }
            });
        }

        function delete_archive(archive_id, callback, type)
        {
            var r = confirm("Are you sure to delete the record!");
            if (r == true) {
                $.ajax({
                    url : APP_BASE_API + "/archive/" + archive_id + "/" + type,
                    type: "DELETE",
                    success:function(data, textStatus, jqXHR)
                    {
                        callback(data);
                        $('.preloader').hide();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $('.preloader').hide();
                    }
                });
            }
        }

        var buildCategory = {
            unbuild : function(data){
                $('#nav-bar').html('');
            },
            build_menu : function(data){
                var menu = buildCategory.build_menu_item(data);
                $('#nav-bar').html(menu);
            },
            build_menu_item : function(data)
            {
                var menu = '';
                $.each(data, function (index, item) {
                    menu += '<li>';

                    if(item.is_leaf.parseBoolean()){
                        menu += '<a onclick="load_archive_data(this,'+item.id+','+item.type_id+')" href="javascript:;">';
                    }
                    else
                        menu += '<a class="dropdown-toggle" href="#">';

                    menu += '<i class="menu-icon fa fa-list-alt"></i>';
                    menu += '<span class="menu-text">' + item.name + '</span>';

                    if(item.children && (item.children.length > 0)){
                        menu += '<b class="arrow fa fa-angle-down"></b>';
                    }
                    menu += '</a>';
                    menu += '<b class="arrow"></b>';
                    if(item.children && (item.children.length > 0)){


                        menu += '<ul class="submenu">';

                        menu += buildCategory.build_menu_item(item.children);

                        menu += '</ul>';
                    }

                    menu += '</li>';
                });
                return menu;
            },
            build : function(){
                $.ajax({
                    url : APP_BASE_API + "/categories",
                    type: "GET",
                    success:function(data, textStatus, jqXHR)
                    {
                        var tree = buildCategory.parse_to_tree(data);

                        buildCategory.build_menu(tree);
                        $( ".ajax-nav-link" ).click(function(event) {
                            event.preventDefault();
                            load_page({"method":"GET","args":{}}, $(this).children('.menu-text').html(),$(this).attr('href'));
    //                        var t = $(this).attr('href');
    //                        var tt = t.split("::");
    //                        switch(tt[0]){
    //                            case "VIEWCODE": load_view(tt[1]);
    //                                break;
    //                            case "DASHBOARD": load_view(tt[1]);
    //                                break;
    //                            case "REPORT": load_view(tt[1]);
    //                                break;
    //                            default:
    //                                load_page({"method":"GET","args":{}}, $(this).children('.menu-text').html(),$(this).attr('href'));
    //                        }
                            return false;
                        });
                        $('.preloader').hide();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $('.preloader').hide();
                    }
                });
            },
            parse_to_tree : function(data)
            {
                var map = {};

                for(var i = 0; i < data.length; i++){
                    var obj = data[i];

                    if((typeof map[obj.id]) === "undefined"){
                        map[obj.id] = obj;
                        obj.children = [];
                    }else{
                        obj.children = map[obj.id].children;
                        map[obj.id] = obj;
                    }

                    var parent = obj.parent_id;
                    if((typeof map[parent]) === "undefined"){
                        map[parent] = {
                            children: []
                        };
                    }
                    map[parent].children.push(obj);
                }

                return map['-1'].children;
            },
        };
        (function(window,undefined){


        })(window);
        jQuery(function($) {
            //buildCategory.build();

            $( ".ajax-nav-link" ).click(function(event) {
                event.preventDefault();
                load_page({"method":"GET","args":{}}, $(this).children('.menu-text').html(),$(this).attr('href'));
                //console.debug(currentDataUrl);
                return false;
            });
        });

        function load_page(argsData, title, url){
            $.ajax({
                url : APP_BASE_VIEW + "/" + url,
                type: argsData.method,
                data : argsData.args,
                success:function(data, textStatus, jqXHR)
                {
                    $('#ajax-content').html(data);
                    $('.preloader').hide();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $('.preloader').hide();
                }
            });
        }

        function breadCrumb(){
            $('.bredcrumb_home').click(function(event){
                event.preventDefault();
            });
        }

        function populatedropdown(node, data, param){
            for (var i = 0; i < data.length; i++) {
                if(data[i].name != ''){
                    node.append('<option '+param+'="'+data[i].id+'">'+data[i].name+'</option>');
                }
            }
        }

        function tabInit(node){
            node.find('.tab-content').children().first().addClass('active').siblings().removeClass('active');
        }

        function randomGenarator(){
            var min = 2; var max = 50;
            return randomNumber = Math.floor(Math.random()*(max-min+1)+min);

        }

        function gritter(title, text, sticky, time, class_name){
            $.gritter.add({
                title: title,
                text: text,
                sticky: sticky,
                time: time,
                class_name: class_name
            });
            return false;
        }

        function fetchPendingdeliveries(mode){
            $.ajax({
                url : url+"/plexpos/api/deliveryStatus/"+mode,
                type: "GET",
                success:function(data, textStatus, jqXHR){
                    updateNotifier(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    gritter('Server Error', 'Server is currently Offline', false, '1500', 'gritter-error, gritter-center');
                }
            });
        }

        function updateNotifier(data){
            $('#onlinenoti_panel').html('');
            updateNoti_count(data.total);
            for(var i = 0; i < data.rows.length; i++){
                var notiContent = '<li> <a href="#" onclick="load_online_order_detail('+data.rows[i].id+');"> <div class="clearfix"> <span id="onlineNoti_address'+i+'" class="pull-left">'+data.rows[i].delivery_address+'</span> <time id="onlineNoti_time'+i+'" class="pull-right" datetime="'+data.rows[i].created_at+'"></time> </div> </a> </li>';
                $('#onlinenoti_panel').append(notiContent);
                $("#onlineNoti_time"+i).timeago();
            }
        }

        function load_online_order_detail(id){
            load_view_n_data("/online_deliveries_details.html","/delivery/" + id + "/" + randomGenarator());
            console.log(id);
        }

        function load_online_order_view(){
            load_view('/online_deliveries.html');
        }

        function updateNoti_count(count){
            $('#online_del_del_count').html(count);
            $('#online_del_del_head').html('<strong>'+count+'</strong> pending online Deliveries!');
        }

        function data_grid_response(data){
            if (data.status == "login") {
                bootbox.confirm("You are not currently Logged-in. Please Log-in to continue..", function(result) {
                    if(result) {
                        window.location.assign("login.html");
                    } else {
                        window.location.assign("login.html");
                    }
                });
            }
            return data;
        }
        // gritter-light gritter-info gritter-success gritter-error

    //    window.onhashchange = function(event) {
    //        console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
    //    };

</script>



</body>
</html>
