<?php
    if( isset($_GET['userEmail']) && isset($_GET['base']) ){
        $email = $_GET['userEmail'];
        $base = $_GET['base'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>plexpos admin</title>
    <link rel="stylesheet" href="/plexpos/assets/css/bootstrap.css" />
    <link rel="stylesheet" href="/plexpos/assets/css/bootstrap-theme.min.css" />
    <script src="/plexpos/assets/js/jquery.js"></script>
    <script src="/plexpos/assets/js/bootstrap.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h1>Congratulations !!</h1>
                    <p>Your registered account with the email address <strong><i><?php echo $email; ?></i></strong> has successfully activated. Now you can log in to the Plexpos Admin Panel with your account <strong>email address</strong> and <strong>password</strong></p>
                    <p><a class="btn btn-primary btn-lg" target="_blank" href=<?php echo $base; ?> role="button">Log in to Plexpos</a></p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>